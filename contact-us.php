<?php include('header.php'); ?>
			<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Contact Us</h1>
								<p>SPONSOR A CHILD AND CHANGE THEIR LIFE FOR <br>GOOD</p>
								<ul>
									<li><a href="index.html">Home</a></li>
									<li><span>-</span></li>
									<li><a href="#">Contact</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Need Our Help</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>

			<!-- Contact Us ____________________________ -->
			<section class="Contact-Us-Pages">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-xs-12">
							<div class="Headquarters-Two">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2366.9840901946955!2d-6.213969283922472!3d53.611586380035476!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x486722d3b1ef3073%3A0xa320bb57ab9c7c2a!2s14+Bremore+Pastures+Cres%2C+Clogheder%2C+Balbriggan%2C+Co.+Dublin%2C+K32+AK75%2C+Ireland!5e0!3m2!1sen!2s!4v1549270847592" width="750" height="290" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<div class="Headquarters-Two">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3617.854034490537!2d67.09804901535433!3d24.937041984017895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb338b1872c8585%3A0x6f6cc1c0e1188fce!2s1%2C+Block+4+Gulshan-e-Iqbal%2C+Karachi%2C+Karachi+City%2C+Sindh%2C+Pakistan!5e0!3m2!1sen!2s!4v1549271091669" width="750" height="290" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
						<div class="col-sm-4 col-sm-8 col-xs-12">
							<div class="Headquarters-Two">
								<h5>Sultan ul Hind Trust (Ireland)</h5>
								<p>14 BREMORE PASTURES CRESCENT, BALBRIGGAN, COUNTY DUBLIN, DUBLIN, Ireland.</p>
								
								<ul class="Toll-Free">
									<!-- <li>Toll Free: 888-299-0338</li> -->
									<li>Phone: +353894894950</li>
									<!-- <li>Fax: 416-362-6669</li> -->
									<li>email: info@sultanulhindtrust.ie</li>
									<!-- <li>E-mail: sultanulhindtrust6@gmail.com</li> -->
								</ul> <!-- /.Toll-Free -->
							</div> <!-- /.Headquarters-Two -->
							<div class="Headquarters-Two" style="    margin-top: 20px;">
								<h5>Sultan ul Hind Trust (Pakistan)</h5>
								<p>A-1/23 Block 4,Gulshan e Iqbal, Karachi, Pakistan.</p>
								<!-- <ul>
									<li>25 York Street</li>
									<li>Suite 900, P.O. Box 403</li>
									<li>Toronto, Ontario</li>
									<li>M5J 2V5</li>
								</ul> -->
								<ul class="Toll-Free">
									<!-- <li>Toll Free: 888-299-0338</li> -->
									<li>Phone: +92-3348221200, +92-3452119787</li>
									<!-- <li>Fax: 416-362-6669</li> -->
									<li>email: sultanulhindtrust6@gmail.com</li>
									<!-- <li>E-mail: sultanulhindtrust6@gmail.com</li> -->
								</ul> <!-- /.Toll-Free -->
							</div> <!-- /.Headquarters-Two -->
							<div class="Client-Care">
								<!-- <h5>Client Care</h5>
								<ul class="Text">
									<li>US/Canada: 800-774-9473</li>
									<li>International: 310-765-3200</li>
									<li>Connect With Us</li>
								</ul> --> <!-- /.Text -->
								<ul class="Icon">
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
								</ul> <!-- /.Icon -->
							</div> <!-- /.Client-Care -->

						</div>
					</div>	
				</div>
			</section>


			

			<!-- Join Volunteer ____________________________ -->
			<section class="Join-Volunteer-Pages margin-bottom">
				<div class="container">
					<div class="Theme-title text-center">
						<h2>Never Hesitate to Reach Out</h2>
						<h6>Contact us</h6>
					</div>
					<form action="inc/sendemail.php" class="form-validation" autocomplete="off">
						<div class="row">
							<div class="col-md-4 col-xs-12">
								<div class="single-input">
									<input type="text" placeholder="You name *" name="name">
								</div> <!-- /.single-input -->
							</div> <!-- /.col -->
							<div class="col-md-4 col-xs-12">
								<div class="single-input">
									<input type="email" placeholder="Email *" name="email">
								</div> <!-- /.single-input -->
							</div> <!-- /.col -->
							<div class="col-md-4 col-xs-12">
								<div class="single-input">
									<input type="text" placeholder="Phone *" name="phone">
								</div> <!-- /.single-input -->
							</div> <!-- /.col -->
							<div class="col-xs-12">
								<div class="single-input">
									<select class="selectpicker" name="category">
										<option>What would you like to do</option>
										<option value="Design & Planting">Join as a volunteer</option>
										<option value="Lawn & Garden Care">Suggest</option>
										<option value="Lawn Moving">Contribute</option>
									</select>
								</div> <!-- /.single-input -->
							</div> <!-- /.col -->
							<div class="col-xs-12">
								<div class="single-textarea">
									<textarea placeholder="Write something about your experience ...." name="message"></textarea>
								</div> <!-- /.single-textarea -->
							</div> <!-- /.col -->
						</div> <!-- /.row -->
						<button class="hvr-float-shadow">Send Request</button>
					</form> <!-- /Form -->
				</div> <!-- /.container -->

				<!--Contact Form Validation Markup -->
				<!-- Contact alert -->
				<div class="alert-wrapper" id="alert-success">
					<div id="success">
						<button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
						<div class="wrapper">
			               	<p>Your message was sent successfully.</p>
			             </div>
			        </div>
			    </div> <!-- End of .alert_wrapper -->
			    <div class="alert-wrapper" id="alert-error">
			        <div id="error">
			           	<button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
			           	<div class="wrapper">
			               	<p>Sorry!Something Went Wrong.</p>
			            </div>
			        </div>
			    </div> <!-- End of .alert_wrapper -->
			</section> <!-- /.Join-Volunteer-Pages -->

<?php include('footer.php'); ?>
			
