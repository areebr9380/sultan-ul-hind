<?php include('config.php'); ?>	
<!DOCTYPE html>
<html lang="en">
	<head>
	
		<meta charset="UTF-8">
		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>SULTAN UL HIND TRUST (Ireland)</title>


		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="images/logo/favicon-1.png">
		
		<!-- Main style sheet -->
		<link rel="stylesheet" href="css/style.css">
		<!-- responsive style sheet -->
		<link rel="stylesheet" href="css/responsive.css">

		<!-- Fix Internet Explorer ______________________________________-->

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="vendor/html5shiv.js"></script>
			<script src="vendor/respond.js"></script>
		<![endif]-->
	</head>

<body>
	
	<?php 

	$home = "/charity/index.php";
	$causes = "/charity/causes.php";
	$about = "/charity/about-us.php";
	$gallery = "/charity/gallery.php";
	$shop = "/charity/shop.php";
	$contact = "/charity/contact-us.php";
	$checker = $_SERVER['REQUEST_URI'];
	$_SERVER['REQUEST_URI'];
	






	?>
	<div class="main-page-wrapper">
		
	<section class="header-section">
				<div class="top-header">
					<div class="container">
						<div class="clear-fix">
							<ul class="float-left top-header-left">
								<li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Program</a></li>
								<li><a href="shop.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Shop</a></li>
								<li><a href="contact-us.php"><i class="fa fa-mobile" aria-hidden="true"></i> Contact</a></li>
							</ul> <!-- /.top-header-left -->
							<ul class="float-right top-header-right">
								<li><a href="https://www.facebook.com/Sultan-UL-Hind-Trust-Ireland-249013369354201/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<!--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
							
								<!--<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>-->
							
								<li><a href="http://www.gmail.com"><i class="fa fa-google" aria-hidden="true"></i></a></li>
							</ul> <!-- /.top-header-right -->
						</div> <!-- /.clear-fix -->
					</div> <!-- /.container -->
				</div> <!-- /.top-header -->
				<div class="middle-header">
					<div class="container">
						<div class="row" style="padding-bottom: 6px;">
							<div class="col-md-4 col-xs-12">
								<div class="them-logo"><a href="index.php"><img src="images/logo/logo.png" alt="logo" style="height: 100px; width: 107px; margin-left: 1px;">
								<div><h4 style="color:#fd580b; margin-left: 120px; margin-top: -60px; font-weight: bold; font-size: 18px;">IRELAND<p style="color:#fd580b;margin: -17px;margin-left: 0px;font-size: 11px;color: #333;">(RCN) : 20200021</p></h4></div>
								</a></div>
								</br><!-- /.them-logo -->
							</div> <!-- /.col -->
							
							<div class="col-md-8 col-xs-12" style="margin-top: 18px;">
								<div class="middle-header-contant">
									<ul class="clear-fix">
										<li>
											<i class="flaticon-clock"></i>
											<p>Monday - Saturday</p>
											<span>10 AM to 06 PM GMT+1</span>
										</li>
				
										<li>
											<i class="flaticon-smartphone"></i>
											<p>Want to talk with us</p>
											<span>+353894894950</span>
										</li>
										<li>
											<i class="flaticon-envelope"></i>
											<p>Send us Email</p>
											<a href="#">info@sultanulhindtrust.ie</a>
										</li>
									</ul> <!-- /.clear-fix -->
								</div> <!-- /.middle-header-contant -->
							</div> <!-- /.col -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.middle-header -->
				
				<!-- Theme Main Menu ____________________________ -->
				<div class="theme-main-menu">
					<div class="container">
						<div class="main-menu menu-skew-div clear-fix">
							<!-- Menu -->
							<nav class="navbar">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed tran3s" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
										<span class="sr-only tran3s">Toggle navigation</span>
										<span class="icon-bar tran3s"></span>
										<span class="icon-bar tran3s"></span>
										<span class="icon-bar tran3s"></span>
									</button>
								</div>
								
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse" id="navbar-collapse-1">
									<ul class="nav navbar-nav">
										<li class="dropdown-holder current-page-item <?php if ($checker==$home) {
											echo  "active";
										} ?> "><a href="index.php">Home</a>
											<ul class="sub-menu">
												<!-- <li><a href="index.php" class="tran3s">Home Version one</a></li>
												<li><a href="index-2.php" class="tran3s">Home Version two</a></li> -->
											</ul>
										</li>

										<li class="dropdown-holder "><a href="showEvents.php">Events</a>
											<ul class="sub-menu">
												<!-- <li><a href="events-v1.php" class="tran3s">Events Version one</a></li>
												<li><a href="events-v2.php" class="tran3s">Events Version two</a></li>
												<li><a href="events-details.php" class="tran3s">Events Details</a></li> -->
											</ul>
										</li>
									   
										<li class="dropdown-holder <?php if ($checker==$causes) {
											echo  "active";
										} ?>"><a href="showCauses.php">Programs</a>
											<ul class="sub-menu">
												<li><a href="showCauses.php" class="tran3s">Causes</a></li>
												<!-- <li><a href="causes-sidebar.php" class="tran3s">Causes Sidebar</a></li> -->
												<!-- <li><a href="causes-details.php" class="tran3s">Causes Details</a></li> -->
											</ul>
										</li>

										<li class="dropdown-holder <?php if ($checker==$about || $checker==$gallery) {
											echo  "active";
										} ?>"><a href="about-us.php">About Us</a>
											<ul class="sub-menu">
												<li><a href="about-us.php" class="tran3s">About</a></li>
												<li><a href="gallery.php" class="tran3s">Gallery</a></li>
												<li><a href="album.php" class="tran3s">Albums</a></li>
												<!-- <li><a href="team.php" class="tran3s">Team</a></li>
												<li><a href="join-volunteer.php" class="tran3s">Join Volunteer</a></li>
												<li><a href="faq.php" class="tran3s">Faq</a></li> -->
											</ul>
										</li>

										<!-- <li class="dropdown-holder"><a href="#">Blog</a>
											<ul class="sub-menu">
												<li><a href="blog-v1.php" class="tran3s">Blog Version one</a></li>
												<li><a href="blog-v2.php" class="tran3s">Blog Version Two</a></li>
												<li><a href="blog-details.php" class="tran3s">Blog Details</a></li>
											</ul>
										</li> -->
									   
										<li class="dropdown-holder <?php if ($checker==$shop) {
											echo  "active";
										} ?>" ><a href="shop.php">Shop</a>
											<ul class="sub-menu">
												<li><a href="shop.php" class="tran3s">Shop</a></li>
												<!-- <li><a href="shop-details.php" class="tran3s">Shop Details</a></li> -->
											</ul>
										</li>
									   
										<li class="<?php if ($checker==$contact) {
											echo  "active";
										} ?>"><a href="contact-us.php">Contact</a></li>
									</ul>
								</div><!-- /.navbar-collapse -->
							</nav>

							<div class="float-right">
								<div class="search-button-content clear-fix">
									<!-- <button class="cart tran3s"><i class="flaticon-shopping-bag"></i> <span>0</span></button> -->
						   			<button class="search tran3s" id="search-button"><i class="flaticon-search"></i></button>
						   			<div class="search-box tran5s" id="searchWrapper">
						   				<button id="close-button" class="p-color"><i class="fa fa-times" aria-hidden="true"></i></button>
						   				<div class="container">
						   					<img src="images/logo/theme-main-logo-1.png" alt="Logo">
						   					<form action="#">
						   						<input type="text" placeholder="Search....">
						   						<button class="p-bg-color"><i class="fa fa-search" aria-hidden="true"></i></button>
						   					</form>
						   				</div>
						   			</div> <!-- /.search-box -->
						   			<a href="donationForm.php" class="a-comon main-menu-button">Donate Now <i class="flaticon-hand"></i></a>
						   		</div> <!-- /.right-content -->
							</div> <!-- /.float-right -->
						</div> <!-- / menu-skew-div -->
					</div> <!-- /.container main-menu -->
				</div> <!-- /.main-menu -->
			</section>