-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 07, 2020 at 01:08 AM
-- Server version: 10.1.43-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dwdm_sultanulhind`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `name`) VALUES
(33, 'Iftari Camps'),
(34, 'Polio Awareness Program'),
(35, 'Relief camp for victims of flood'),
(37, 'SETNS'),
(36, 'Camp- Eid e Milad un Nabi(SAW)'),
(32, 'Charities Regulator Event'),
(31, 'Christmas Presents for homeless'),
(30, 'Food Assistance for homeless');

-- --------------------------------------------------------

--
-- Table structure for table `causes`
--

CREATE TABLE `causes` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `country` text NOT NULL,
  `image` int(100) NOT NULL,
  `ext` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `causes`
--

INSERT INTO `causes` (`id`, `title`, `country`, `image`, `ext`, `description`, `status`) VALUES
(1, 'Support a child', 'Karachi Pakistan', 1, 'jpg', 'Support needy children', 1),
(2, 'Health Assistance Program', 'Ireland, Pakistan, Somalia, Syria, Yemen, Others', 2, 'jpg', 'Health Assistance Program Description', 1),
(3, 'Assistance For Homeless', 'Ireland, Pakistan, Somalia, Others', 3, 'jpg', 'Homeless People', 1),
(4, 'Nicola\'s Fundraising Appeal', 'Dublin, Ireland', 4, 'jpeg', 'Nicola\'s Fundraising Appeal....\r\n</br>\r\nNicola is a very dear close friend, the nicest person you could meet with a 20 month old little boy called Cal. She is battling Stage 4 cancer so that she can stay alive to see her little boy grow up. Nicola is an exceptionally private person and it was a massive decision for her to make her appeal public just before Christmas on social media, radio and in the National Newspapers in order to raise funds to have pioneering and life saving treatment in Thailand. Please take a few minutes to view Nicola\'s video here:</br>\r\n<a target=\'_blank\' href=\'https://www.youtube.com/watch?v=BTYcHiN3_OQ#action=share\'>https://www.youtube.com/watch?v=BTYcHiN3_OQ#action=share</a>\r\n</br>\r\nTo assist Nicola in raising the necessary funds to pay for this life saving treatment the following GofundMe page has been set up:\r\n<a href=\'https://www.gofundme.com/f/nicolahanney\' target=\'_blank\'>https://www.gofundme.com/f/nicolahanney</a>\r\n</br>\r\nIn the 6 weeks since the GoFundMe page has been set up 77,000 Cent of the 200,000 cent target has been raised through the generosity of the general public.\r\nIn addition to this we are also planning and running a number of fundraising events to achieve the target and any support that could provide would be gratefully appreciated.\r\n</br>\r\nOn the 14th December Nicola made the heart wrenching decision to leave her little boy and all her family and friends to travel alone to Thailand to start her treatment. She made this short video when leaving Dublin to thank her supporters\r\n</br>\r\nNicola is now 3 weeks into a 12 week treatment program, In order for her to complete this life saving treatment in full we still need to reach the 200,000 cent target as quickly as possible.\r\n</br>\r\nWe would be extremely grateful for any support can provide in sharing Nicola\'s story on facebook ,Instagram, twitter , and helping us get her back home happy and healthy to her little boy.\r\nyou can also donate from our website <a href=\'http://sultanulhindtrust.ie/\'>www.sultanulhindtrust.ie</a>\r\n</br>\r\nPlease see below links to Nicola\'s newspaper article in the Irish Independent and her radio interview on 98FM.\r\n</br>\r\n<a href=\'https://www.independent.ie/life/health-wellbeing/it-breaks-my-heart-that-my-son-might-not-remember-me-mother-with-stage-four-cancer-fundraising-for-pioneering-treatment-38756817.html\' target=\'_blank\'>https://www.independent.ie/.../it-breaks-my-heart-that-my-son...</a>\r\n</br>\r\n<a href=\'https://www.98fm.com/podcasts/98fm-39-s-dublin-talks/dublin-mums-heartbrey\' target=\'_blank\'>https://www.98fm.com/.../98fm-39-s-dubl.../dublin-mums-heartbrey</a> thanks again for your help.\r\n</br>\r\nFARAZ KHAN\r\n</br>\r\n0894894950', 1);

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE `donation` (
  `id` int(255) NOT NULL,
  `cause` varchar(250) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `country` text NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `uniqid` varchar(250) NOT NULL,
  `status` int(250) NOT NULL DEFAULT '0',
  `amount` varchar(250) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`id`, `cause`, `full_name`, `email`, `country`, `address`, `city`, `postal_code`, `uniqid`, `status`, `amount`, `date_time`) VALUES
(1, '3', 'faraz Khan', '', 'Ireland', '14 bremore pastures, Crescent', 'Balbriggan', 'K32AK75', '79a7e5c16c82a8a5267faf4551a10b51,754487', 0, '', '2019-02-25 16:47:09'),
(2, '3', 'faraz khan', '', 'Ireland', '14 bremore pastures cresents', 'balbriggan', 'k32AK75', '87bdd803e3e35f2d6e5b02703d7d6e57,479913', 0, '', '2019-02-28 19:52:54'),
(3, '3', 'faraz khan', '', 'Ireland', '14 bremore pastures cresents', 'balbriggan', 'k32AK75', '850273d934f5e8d1aba00bacc996c618,669959', 0, '', '2019-02-28 20:01:08'),
(4, '3', 'faraz Khan', '', 'Ireland', '14 bremore pastures, Crescent', 'Balbriggan', 'K32AK75', '109f2ba0bda1e78dbd824fbf18df1aa1,374283', 1, '2.00', '2019-03-07 23:51:32'),
(5, '3', 'Sheraz ikram Khan ', '', 'Pakistan', 'Metrovill 3rd', 'Karachi', '050', '588ece68e5e771435cb93fc531142b48,552462', 0, '', '2019-03-08 10:31:02'),
(6, '1', 'Aeeb', '', 'Pakistan', 'testing', 'Karachi', '123', '523d51652051de940a5fb53a0bb1c3cf,540049', 0, '', '2019-12-03 05:00:26'),
(7, '6', 'faraz', '', 'Afghanistan', '14 bremore pastures', 'Balbriggan', 'K32AK75', '69680d6e8d52d37091b58b3aa602f8c3,214299', 1, '0.01', '2019-12-03 08:35:37'),
(8, '12', 'faraz khan', '', 'Ireland', '14 bremore pasture crescent', 'balbriggan', 'K32AK75', '871eb5d77ade69619ed05503ff9ac4dc,713484', 0, '', '2020-02-03 23:04:13');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `designation` varchar(255) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `password` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `emp_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emp_type`
--

CREATE TABLE `emp_type` (
  `id` int(11) NOT NULL,
  `emp_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_type`
--

INSERT INTO `emp_type` (`id`, `emp_type`, `status`) VALUES
(1, 'Admin', 1),
(2, 'Employee', 1);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` int(100) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `venue` text NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `image`, `extension`, `date`, `start`, `end`, `venue`, `description`, `location`, `status`) VALUES
(1, 'Charities Regulator\'s event on Charity Trustees\' Week - Academy Plaza Hotel, Dublin', 1, 'jpg', '2018-11-13', '20:00:00', '22:00:00', 'Charities Regulator\'s event on Charity Trustees\' Week - Academy Plaza Hotel, Dublin', 'Charities Regulator\'s event on Charity Trustees\' Week - Academy Plaza Hotel, Dublin', '', 1),
(2, 'Food Assistance for Homeless - Dublin Ireland', 2, 'jpg', '2018-12-19', '20:00:00', '22:00:00', 'Dubline Ireland', 'Food Assistance for Homeless - Dubline Ireland', '', 1),
(3, 'Christmas Presents for Homeless - Dublin Ireland', 5, 'jpg', '2018-12-25', '12:59:00', '00:59:00', 'Dubline Ireland', 'Christmas Presents for Homeless - Dubline Ireland', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3620.479093820411!2d67.02317741535309!3d24.84748158405996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb33ddfa27f7849%3A0xc2fbc4a56857f36a!2sPearl+Continental+Hotel!5e0!3m2!1sen!2s!4v1543068648995\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `general_cause`
--

CREATE TABLE `general_cause` (
  `gcause_id` int(11) NOT NULL,
  `gcause_title` varchar(60) NOT NULL,
  `gcause_description` varchar(500) NOT NULL,
  `gcause_country` varchar(50) NOT NULL,
  `gcause_img` int(11) NOT NULL,
  `gcause_img_ext` varchar(50) NOT NULL,
  `gcause_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_cause`
--

INSERT INTO `general_cause` (`gcause_id`, `gcause_title`, `gcause_description`, `gcause_country`, `gcause_img`, `gcause_img_ext`, `gcause_status`) VALUES
(3, '2 â‚¬ Meal for Pakistan', 'Meal for Pakistan', 'Pakistan', 8, 'jpg', 1),
(2, 'Food Packs for Pakistan', 'Food Packs for Pakistani needy people', 'Pakistan', 2, 'jpg', 1),
(4, 'Zakat', 'Zakat for needy people', 'Pakistan', 4, 'jpg', 1),
(5, 'Land for Graveyard Pakistan', 'land for graves', 'Pakistan', 5, 'jpg', 1),
(6, 'Support for homeless people Ireland', 'Ireland', 'Ireland', 6, 'jpg', 1),
(7, 'Food Packs For Other Countries', 'Food Packs For Ireland', 'Ireland', 7, 'jpg', 1),
(1, 'Dig A Well', 'Dig a well for water', 'Pakistan', 3, 'jpg', 1),
(10, 'Nicolas Fundraising Appeal', 'Nicolas Fundraising Appeal....\r\n\r\nNicola is a very dear close friend, the nicest person you could meet with a 20 month old little boy called Cal. She is battling Stage 4 cancer so that she can stay alive to see her little boy grow up. Nicola is an exceptionally private person and it was a massive decision for her to make her appeal public just before Christmas on social media, radio and in the National Newspapers in order to raise funds to have pioneering and life saving treatment in Thailand. P', 'Diblin, Ireland', 9, 'png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image` int(11) NOT NULL,
  `ext` varchar(10) NOT NULL,
  `album_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image`, `ext`, `album_id`) VALUES
(159, 47, 'jpg', 34),
(158, 46, 'jpg', 34),
(157, 45, 'jpg', 34),
(156, 44, 'jpg', 34),
(155, 43, 'jpg', 34),
(154, 42, 'jpg', 34),
(153, 41, 'jpg', 33),
(152, 40, 'jpg', 33),
(151, 39, 'jpg', 33),
(150, 38, 'jpg', 33),
(149, 37, 'jpg', 33),
(148, 36, 'jpg', 33),
(147, 35, 'jpg', 33),
(146, 34, 'jpg', 33),
(145, 33, 'jpg', 33),
(144, 32, 'jpg', 33),
(143, 31, 'jpg', 33),
(142, 30, 'jpg', 33),
(141, 29, 'jpg', 33),
(140, 28, 'jpg', 33),
(139, 27, 'jpg', 32),
(138, 26, 'jpg', 32),
(137, 25, 'jpg', 32),
(136, 24, 'jpg', 32),
(135, 23, 'jpg', 31),
(134, 22, 'jpg', 31),
(133, 21, 'jpg', 31),
(132, 20, 'jpg', 31),
(131, 19, 'jpg', 31),
(130, 18, 'jpg', 30),
(129, 17, 'jpg', 30),
(128, 16, 'jpg', 30),
(127, 15, 'jpg', 30),
(126, 14, 'jpg', 30),
(125, 13, 'jpg', 30),
(124, 12, 'jpg', 30),
(123, 11, 'jpg', 30),
(122, 10, 'jpg', 30),
(121, 9, 'jpg', 30),
(120, 8, 'jpg', 30),
(119, 7, 'jpg', 30),
(118, 6, 'jpg', 30),
(117, 5, 'jpg', 30),
(116, 4, 'jpg', 30),
(115, 3, 'jpg', 30),
(114, 2, 'jpg', 30),
(113, 1, 'jpg', 30),
(160, 48, 'jpg', 34),
(161, 49, 'jpg', 35),
(162, 50, 'jpg', 35),
(163, 51, 'jpg', 35),
(164, 52, 'jpg', 35),
(165, 53, 'jpg', 35),
(166, 54, 'jpg', 35),
(167, 55, 'jpg', 35),
(168, 56, 'jpg', 35),
(169, 57, 'jpg', 35),
(170, 58, 'jpg', 35),
(171, 59, 'jpg', 35),
(172, 60, 'jpg', 35),
(173, 61, 'jpg', 35),
(174, 62, 'jpg', 35),
(175, 63, 'jpg', 35),
(176, 64, 'jpg', 35),
(177, 65, 'jpg', 35),
(178, 66, 'jpg', 35),
(179, 67, 'jpg', 35),
(180, 68, 'jpg', 35),
(181, 69, 'jpg', 35),
(182, 70, 'jpg', 35),
(183, 71, 'jpg', 35),
(184, 72, 'jpg', 35),
(185, 73, 'jpg', 35),
(186, 74, 'jpg', 35),
(187, 75, 'jpg', 36),
(188, 76, 'jpg', 36),
(189, 77, 'jpg', 36),
(190, 78, 'jpg', 36),
(191, 79, 'jpg', 36),
(192, 80, 'jpg', 36),
(193, 81, 'jpg', 36),
(194, 82, 'jpg', 36),
(195, 83, 'jpg', 36),
(196, 84, 'jpg', 36),
(197, 85, 'jpg', 36),
(198, 86, 'jpg', 36),
(199, 87, 'jpg', 36),
(200, 88, 'jpg', 36),
(201, 89, 'jpg', 36),
(202, 90, 'jpg', 36),
(203, 91, 'jpg', 36),
(204, 92, 'jpg', 36),
(205, 93, 'jpg', 36),
(206, 94, 'jpg', 36),
(207, 95, 'jpg', 36),
(208, 96, 'jpg', 36),
(209, 97, 'jpg', 36),
(210, 98, 'jpg', 37);

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `id` int(11) NOT NULL,
  `monthName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`id`, `monthName`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `issuer` int(11) NOT NULL,
  `card_number` int(11) NOT NULL,
  `secure_code` int(11) NOT NULL,
  `expiry_month` int(11) NOT NULL,
  `expiry_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'employee');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `image` int(100) NOT NULL,
  `extension` varchar(225) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `image`, `extension`, `status`) VALUES
(1, 'Homeless People', 3, 'png', 1),
(2, 'Land for Graveyard ', 2, 'png', 1),
(3, 'Child Support', 4, 'jpg', 1),
(4, 'Food Assistance', 1, 'png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcauses`
--

CREATE TABLE `subcauses` (
  `subcause_id` int(11) NOT NULL,
  `subcause_title` varchar(50) NOT NULL,
  `cause_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcauses`
--

INSERT INTO `subcauses` (`subcause_id`, `subcause_title`, `cause_id`) VALUES
(1, 'Support a child education', 1),
(2, 'Children necessities', 1),
(3, 'Children Health', 1),
(4, 'Malnutrition', 2),
(5, 'Provision of Medicines', 2),
(6, 'Support for treatment of diseases', 2),
(7, 'Shelter', 3),
(8, 'Blankets', 3),
(9, 'Households necessities', 3),
(12, 'Donate Nicola', 4);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cell_number` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `post_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(225) NOT NULL,
  `cell_number` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(10) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `f_name`, `lname`, `email`, `cell_number`, `password`, `address`, `city`, `country`, `role_id`, `status`) VALUES
(1, 'ptnest', 'org', 'admin@sultanulhind.com', '0333-2221111', 'admin123', 'Karachi Pakistan', 'Karachi', 'Pakistan', 1, 1),
(2, 'Hassan', 'Khan', 'hk@gmail.com', '030212144', 'asd-123', 'Johar', 'Karachi', 'Paksitan', 2, 1),
(3, 'Ali', 'Khan', 'ali@gmail.com', '0300-21365478', '123456789', 'Nazimabad', 'Karachi', 'Pakistan', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `causes`
--
ALTER TABLE `causes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donation`
--
ALTER TABLE `donation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniqid` (`uniqid`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_type`
--
ALTER TABLE `emp_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_cause`
--
ALTER TABLE `general_cause`
  ADD PRIMARY KEY (`gcause_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcauses`
--
ALTER TABLE `subcauses`
  ADD PRIMARY KEY (`subcause_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `causes`
--
ALTER TABLE `causes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `donation`
--
ALTER TABLE `donation`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `emp_type`
--
ALTER TABLE `emp_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `general_cause`
--
ALTER TABLE `general_cause`
  MODIFY `gcause_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;

--
-- AUTO_INCREMENT for table `month`
--
ALTER TABLE `month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subcauses`
--
ALTER TABLE `subcauses`
  MODIFY `subcause_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
