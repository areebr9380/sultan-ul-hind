<?php include('header.php'); ?>
			<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<!-- <h1>About Us</h1>
								<p>SPONSOR A CHILD AND CHANGE THEIR LIFE FOR <br>GOOD</p> -->
								<!-- <ul>
									<li><a href="index.html">Home</a></li>
									<li><span>-</span></li>
									<li><a href="#">About us</a></li>
								</ul> -->
								<!-- <a href="#" class="hvr-bounce-to-right">Need Our Help</a> -->
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>

			<section>
				<div class="container">
					<div>
						<H3 style="text-align: center; margin-top: 23px; font-weight: 700;color: #fd580b; font-size: 40px;">
							ABOUT SULTAN UL HIND TRUST (IRELAND)</H3><hr>
					</div>
					<div style="text-align: justify;font-weight: 600;">
						<p>Sultan ul Hind Trust is a private charitable trust attributed to Great Sufi Saint Hazrat Khwaja Moin uddin Hassan
						 Chishti popularly known as Hazrat Khawaja Gharib Nawaz (R.A). Trust is registered in charity regulator having 
						 Registered Charity Number (RCN): 20200021.  Sultan Ul Hind Trust has been registered in Pakistan Since 2006. We have been serving people in term of food,shelter,medicine,health and others</p><br>

						<p>The main object for which the Body is established is to provide assistance to those experiencing extreme hardship
						  or severe ill health in Ireland, Pakistan and across the globe, irrespective of religion, cast or creed. The trust will
						   support causes based on applications submitted to, reviewed and approved by the board of trustees. This include funding 
						   medical treatment, funding education, providing food camps, providing cloths and  providing shelter in addition to raising 
						   awareness of different causes in Ireland , Pakistan and across the world.</p><br>

						<p>Provision of land for graveyards is one of the prime objective.</p><br>

						<p>Our ambition is to reach those segments of society who are unreached, underprivileged, neglected and inappropriately served
						 in terms of health, education, food, shelter and justice, and help them to fulfil their needs of basic necessities of life thus
						 raising their standards of quality living irrespective of cast, creed, gender, religious affiliation, age, physical appearance and language.</p><br>

						<p>Our aim is also to encourage the sufferings for self-reliance through proper guidance care and concern and give them structured approach to life.
						So that they can stand on their own and can reshape their patterns for living as to be successful not only in this world but also here after.</p><br>

						<p>Our motive is not only confined to those segments of society who always seek others for help but also to those who are financially established and 
						have everything which one might dream of living with comfort but yet they have not recognized the true objective of their sending in this world.</p><br>
					</div>
					<div>
						<h4 style="text-align: left; margin-top: 23px;color: #fd580b; font-weight: 700;">Charitable Purposes:</h4><hr>
					</div>
					<div style="margin-left: 44px; padding-bottom: 30px;">
						<ul style="font-family: 'Open Sans', sans-serif; font-weight: 400; line-height: 40px; font-size: 16px; color: rgba(0,0,0,0.7);list-style-type:disc !important;">
							<li>Relief of poverty or economic hardship</li>
							<li>Advancement of education</li>
							<li>Other purpose that is of benefit to the community</li>
							<li>Advancement of community welfare including the relief of those in need by reason of youth, age, ill-health, or disability</li>
							<li>Promotion of civic responsibility or voluntary work</li>
							<li>Promotion of health, including the prevention or relief of sickness, disease or human suffering</li>
							<li>Promotion of religious or racial harmony and harmonious community relations</li>
							<li>Integration of those who are disadvantaged, and the promotion of their full participation, in society</li>
						</ul>
					</div>
				</div>

			</section>
						

			
			
			
							

			
<?php include('footer.php'); ?>