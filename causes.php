<?php include('header.php'); ?>
				
			<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Causes with 3 column</h1>
								<p>SPONSOR A CHILD AND CHANGE THEIR LIFE FOR <br>GOOD</p>
								<ul>
									<li><a href="index.html">Home</a></li>
									<li><span>-</span></li>
									<li><a href="#">Causes, Campaigns</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Need Our Help</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>

			

			<!-- Rcent Causes ____________________________ -->
			<section class="Rcent-Causes-Section padding-bottom-0">
				<div class="container">
					<div class="Rcent-Causes-Item-Wrapper">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="Causes-Item Causes-Item-margin">
									<?php 
	$cause = mysqli_query($conn,"SELECT * FROM causes where status = 1");
	while ($causeRow = mysqli_fetch_array($cause)) {
		echo('									
						<div class="item">
							<div class="Causes-Item">
								<div class="Causes-Img"><img src="causes/'.$causeRow['image'].".".$causeRow['ext'].'" alt="image"></div> <!-- /.Causes-Img -->
									<div class="Causes-Text">
										<h3>'.$causeRow['title'].'<br>'."(".$causeRow['country'].")".'</h3>
										<ul>
											<li>Donated</li>
											<li>
												<div class="donate-piechart tran3s">
									                <div>
													  
													</div>
									            </div> <!-- /.donate-piechart -->
											</li>
											<li>€ 1600 to Go</li>
										</ul>
										<p>'.$causeRow['description'].'</p>
										<a href="causes-details.html">Donate Now</a>
									</div> <!-- /.Causes-Text -->
								</div> <!-- /.Causes-Item -->
							</div> <!-- /.item -->
');
	}
?>
								</div> <!-- /.Causes-Item -->
							</div> <!-- /.col -->
						</div> <!-- /.row -->
						<a href="#" class="hvr-float-shadow margin-top-0">Load more Causes</a>
					</div> <!-- /.Rcent-Causes-Item-Wrapper -->
				</div> <!-- /.container -->
			</section> <!-- /.Rcent-Causes-Section -->
			
			<!-- Children Care List  _________________________________ -->
			<section class="Children-Care-list-margin">
				<div class="container">
					<div class="Children-Care-list">
						<div id="Children-Care-List-Slider" class="owl-carousel owl-theme">
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-handshake"></i>
									<h6><a href="#">Children’s Care</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation-1"></i>
									<h6><a href="#">Donate</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation"></i>
									<h6><a href="#">Volunteer</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation-2"></i>
									<h6><a href="#">Food assistance</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
						</div> <!-- / #Children-Care-List-Slider -->
					</div> <!-- /.Children-Care-list -->
				</div> <!-- /.container -->
			</section> <!-- /.Children-Care-list-margin -->

<?php include('footer.php'); ?>