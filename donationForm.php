
<?php include('header.php'); ?>

<style type="text/css" media="screen">
.tabcon li a {
    background-color: #2b2a27;
    color: white;
}

.nav-tabs>li.active>a {
    color: #fff !important;
    cursor: default  !important;
    background-color: #fd580b  !important;
    border: 1px solid #ddd  !important;
    border-bottom-color: transparent  !important;
}
</style>
 
<div class="main-page-wrapper" style="    background: url(./images/donation_image/hands-poor-poverty-9749.jpg);
    background-size: 100% 100%;     padding: 40px;">
  <div class="top-headings"><center><h2  style="    margin-bottom: 10px;
    color: #fd580b;">Make a Donation</h2></center></div>



<div class="container tabcon">
 
  <ul class="nav nav-tabs">
    <li class="active"><a href="#home">Single Donation</a></li>
    <li><a href="#menu1">Monthly Donation</a></li>
   
  </ul>

  <div class="tab-content">




  
<div id="home" class="tab-pane fade in active" style="    background: #fd580b66;
    padding: 15px;">
    

      <div class="row">



      <div class="col-md-6">
  <div class="row">

<?php
$date_check = date("M,d,Y h:i:s A"); 
$uni_check=rand(10,1000000);
?>

<form class="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" id="paypal_form">
    <input type="hidden" name="cmd" value="_s-xclick" />
    
<input type="hidden" name="hosted_button_id" value="UM3X57GS3NQCJ" />
        <div class="col-sm-4" >
          <b>Specify Amount</b> <br/><br/>
          <input type="hidden" name="textDonationSingle" value="single">
          <input type="hidden" name="custom" value="<?php echo md5($date_check).','.$uni_check; ?>" id="uniqid">
         

     <input class="form-control" type="text"  placeholder="Amount"  name="amount">
        </div>
      <div class="col-sm-4">
          <b>Currency</b> <br/><br/>

     <select class="form-control" name="txtSCurrency">
       <option value="">Select Currency</option>
       <?php
        showCurrency();
       ?>
     </select>
        </div>
        <div class="col-sm-4">    
          <b>Choose Cause</b> <br/><br/>
 
                <select class="form-control" name="sig" id="txtSCause">
                    
<?php
$cname="Select a country";
$cid="";
if(isset($_GET['donation_type'])){
  $causetype=$_GET['causetype'];
  $donationId = $_GET['donation_type'];
  if($causetype=="generalcause")
  {
     $query = mysqli_query($conn,"SELECT * FROM general_cause where gcause_id='".$donationId."'");
     while($row=mysqli_fetch_array($query)){
       $cname=$row[3];
        $cid=$row[3];
       ?>
       <option value="<?php echo $row['gcause_id'];?>"><?php echo $row['gcause_title'];?></option>
     <?php
     }
     } 
     if($causetype=="slider"){

      $query = mysqli_query($conn,"SELECT * FROM slider where id='".$donationId."'");
     while($row=mysqli_fetch_row($query)){
       $cname=$row[3];
        $cid=$row[3];
       ?>
       <option value="<?php echo $row[0];?>"><?php echo $row[1];?></option>
     <?php
     }
     }
     if($causetype=="causes"){

      $query = mysqli_query($conn,"SELECT * FROM subcauses where cause_id='".$donationId."'");
     while($row=mysqli_fetch_row($query)){
       $cname=$row[3];
        $cid=$row[3];
       ?>
       <option value="<?php echo $row[0];?>"><?php echo $row[1];?></option>
     <?php
     }
     

     }
}






else{


  $query1 = mysqli_query($conn,"SELECT * FROM subcauses");
  while($row1=mysqli_fetch_row($query1)){
   
    ?>
    <option value="<?php echo $row1[0];?>"><?php echo $row1[1];?></option>
  <?php
  }
  $query="SELECT * FROM general_cause WHERE gcause_status='1'";  
  $cmd=mysqli_query($conn,$query);
  while ($row=mysqli_fetch_row($cmd)) {
    print "<option value='$row[0]'>$row[1]</option>";
  }


}
?>
                  </select>
        </div>

         





      </div>


<div class="row">
<div class="col-md-12">    <br>
     <input class="form-control" type="text"  placeholder="Full Name"  id="txtSFName"  name="txtSFName" > </div>
</div>


<div style="height: 30px;"></div>
<div class="row">
<div class="col-md-12">    
     <select class="form-control"  name="txtSFCountry" id="txtSFCountry" >
      <?php
      $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
       
       for($b=0;$b<sizeof($countries);$b++){
        print "<option value='$countries[$b]'>$countries[$b]</option>";
       }
       ?>
      
       
     </select>
      

     
     <br>
      <input class="form-control" type="text"  placeholder="Address"  name="txtSAddress" id="txtSAddress" >  
      <br>
       <input class="form-control" type="text"  placeholder="City" name="txtSCity"  id="txtSCity" > 
       <br>
     <input class="form-control" type="text"  placeholder="Postal code"   name="txtSPostalCode"  id="txtSPostalCode"> 
     
    
     <br>
     



       </div>
      
</div>


      </div>



<div class="col-md-6" style="border-radius: 25px;background:white;opacity:0.6;corner-radius:5px;border: 2px solid red;    margin-left: -5px;margin-top: 74px;">
    <h3 style="text-align:center;"><b>Account Info.</b></h3></br>
<label><b>Bank:</b></label> Allied Irish Banks, p.l.c, Swords Co Dublin Branch, Dublin – Ireland.</br>
<label><b>Title of account:</b></label> Sultan Ul Hind Trust</br>
<label><b>National Sort:</b></label> 93-25-23</br>
<label><b>Account No:</b></label> 64474-044</br>
<label><b>BIC No:</b></label> AIBKIE2D</br>
<label><b>IBAN No:</b></label> IE63 AIBK 9325 2364 4740 44</br></br></br></br>


</div>


</div>

<div style="height: 30px;"></div>
 <!--<input type="hidden" name="cmd" value="_xclick" />-->
 <!--       <input type="hidden" name="no_note" value="1" />-->
 <!--       <input type="hidden" name="lc" value="UK" />-->
 <!--       <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />-->
 <!--       <input type="hidden" name="first_name" value="Customer's First Name" />-->
 <!--       <input type="hidden" name="last_name" value="Customer's Last Name" />-->
 <!--       <input type="hidden" name="payer_email" value="customer@example.com" />-->
 <!--       <input type="hidden" name="item_number" value="123456" / >-->
        
        <!--<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="btnSdonate" title="PayPal - The safer, easier way to pay online!"-->
        <!--alt="Donate with PayPal button" />-->

<!--<img alt="" border="0" src="https://www.sandbox.paypal.com/en_IE/i/scr/pixel.gif" width="1" height="1" />-->

        <input type="button" name="btnSdonate" value="Submit Payment" onClick='donate_amount()' class="btn btn-primary" style="background:#fd580b;    margin-left: -568px;margin-left: 10px;" />   
        

<?php
  if (isset($_POST['btnSdonate'])) {
    $insertQuery = mysqli_query($conn, "INSERT INTO donation VALUES(null,'".$_POST['txtSAmount']."','".$_POST['txtSCurrency']."','".$_POST['txtSCause']."','".$_POST['txtSFName']."',
    '".$_POST['txtSCountry']."','".$_POST['txtSAddress']."','".$_POST['txtSCity']."','".$_POST['txtSPostalCode']."','".$_POST['textDonationSingle']."',NOW())");
  }
  
?>
    </div>  
</form>



 
           





<!-- //menu1 start -->

    <div id="menu1" class="tab-pane fade" style="    background:#fd580b66;
    padding: 15px;">
         

      <div class="row">



      <div class="col-md-6">
  <div class="row">


<form class="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" id="paypal_forms">
    <input type="hidden" name="cmd" value="_s-xclick" />
    
<input type="hidden" name="hosted_button_id" value="UM3X57GS3NQCJ" />
        <div class="col-sm-4" >
          <b>Specify Amount</b> <br/><br/>
          <input type="hidden" name="textDonationSingle" value="single">
          <input type="hidden" name="custom" value="<?php echo md5($date_check).','.$uni_check; ?>" id="uniqid">
         

     <input class="form-control" type="text"  placeholder="Amount"  name="amount">
        </div>
        
      <div class="col-sm-4">
          <b>Currency</b> <br/><br/>

     <select class="form-control" name="txtSCurrency">
       <option value="">Select Currency</option>
       <?php
        showCurrency();
       ?>
     </select>
        </div>
        <div class="col-sm-4">    
          <b>Choose Cause</b> <br/><br/>
 
                <select class="form-control"  name="sig" id="txtSCause">
                    
<?php
$cname="Select a country";
$cid="";
if(isset($_GET['donation_type'])){
  $causetype=$_GET['causetype'];
  $donationId = $_GET['donation_type'];
  if($causetype=="generalcause")
  {
     $query = mysqli_query($conn,"SELECT * FROM general_cause where gcause_id='".$donationId."'");
     while($row=mysqli_fetch_array($query)){
       $cname=$row[3];
        $cid=$row[3];
       ?>
       <option value="<?php echo $row['gcause_id'];?>"><?php echo $row['gcause_title'];?></option>
     <?php
     }
     } 
     if($causetype=="slider"){

      $query = mysqli_query($conn,"SELECT * FROM slider where id='".$donationId."'");
     while($row=mysqli_fetch_row($query)){
       $cname=$row[3];
        $cid=$row[3];
       ?>
       <option value="<?php echo $row[0];?>"><?php echo $row[1];?></option>
     <?php
     }
     }
     if($causetype=="causes"){

      $query = mysqli_query($conn,"SELECT * FROM subcauses where cause_id='".$donationId."'");
     while($row=mysqli_fetch_row($query)){
       $cname=$row[3];
        $cid=$row[3];
       ?>
       <option value="<?php echo $row[0];?>"><?php echo $row[1];?></option>
     <?php
     }
     

     }
}






else{


  $query1 = mysqli_query($conn,"SELECT * FROM subcauses");
  while($row1=mysqli_fetch_row($query1)){
   
    ?>
    <option value="<?php echo $row1[0];?>"><?php echo $row1[1];?></option>
  <?php
  }
  $query="SELECT * FROM general_cause WHERE gcause_status='1'";  
  $cmd=mysqli_query($conn,$query);
  while ($row=mysqli_fetch_row($cmd)) {
    print "<option value='$row[0]'>$row[1]</option>";
  }


}
?>
                  </select>
        </div>

         





      </div>


<div class="row">
<div class="col-md-12">    <br>
     <input class="form-control" type="text"  placeholder="Full Name"  id="txtSFName"  name="txtSFName" > </div>
</div>


<div class="row">
<div class="col-md-12">    <br>
     <input class="form-control" type="gmail"  placeholder="abc@gmail.com" id="txtSgmail"  name="txtSgmail" > </div>
</div>


<div style="height: 30px;"></div>
<div class="row">
<div class="col-md-12">    
     <select class="form-control"  name="txtSFCountry" id="txtSFCountry" >
      <?php
      $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
       
       for($b=0;$b<sizeof($countries);$b++){
        print "<option value='$countries[$b]'>$countries[$b]</option>";
       }
       ?>
      
       
     </select>
      

     
     <br>
      <input class="form-control" type="text"  placeholder="Address"  name="txtSAddress" id="txtSAddress" >  
      <br>
       <input class="form-control" type="text"  placeholder="City" name="txtSCity"  id="txtSCity" > 
       <br>
     <input class="form-control" type="text"  placeholder="Postal code"   name="txtSPostalCode"  id="txtSPostalCode"> 
     
    
     <br>
     



       </div>
      
</div>


      </div>



<div class="col-md-6" style="border-radius: 25px;background:white;opacity:0.6;corner-radius:5px;border: 2px solid red;    margin-left: -5px;margin-top: 74px;">
    <h3 style="text-align:center;"><b>Account Info.</b></h3></br>
<label><b>Bank:</b></label> Allied Irish Banks, p.l.c, Swords Co Dublin Branch, Dublin – Ireland.</br>
<label><b>Title of account:</b></label> Sultan Ul Hind Trust</br>
<label><b>National Sort:</b></label> 93-25-23</br>
<label><b>Account No:</b></label> 64474-044</br>
<label><b>BIC No:</b></label> AIBKIE2D</br>
<label><b>IBAN No:</b></label> IE63 AIBK 9325 2364 4740 44</br></br></br></br>


</div>


</div>

<div style="height: 30px;"></div>
 <!--<input type="hidden" name="cmd" value="_xclick" />-->
 <!--       <input type="hidden" name="no_note" value="1" />-->
 <!--       <input type="hidden" name="lc" value="UK" />-->
 <!--       <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />-->
 <!--       <input type="hidden" name="first_name" value="Customer's First Name" />-->
 <!--       <input type="hidden" name="last_name" value="Customer's Last Name" />-->
 <!--       <input type="hidden" name="payer_email" value="customer@example.com" />-->
 <!--       <input type="hidden" name="item_number" value="123456" / >-->
        
        <!--<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="btnSdonate" title="PayPal - The safer, easier way to pay online!"-->
        <!--alt="Donate with PayPal button" />-->

<!--<img alt="" border="0" src="https://www.sandbox.paypal.com/en_IE/i/scr/pixel.gif" width="1" height="1" />-->

        <input type="button" name="btnSdonate" value="Submit Payment" onClick='donate_monthly_amount()' class="btn btn-primary" style="background:#fd580b;    margin-left: -568px;margin-left: 10px;" />   

<?php
  if (isset($_POST['btnSdonate'])) {
    $insertQuery = mysqli_query($conn, "INSERT INTO donation VALUES(null,'".$_POST['txtSAmount']."','".$_POST['txtSCurrency']."','".$_POST['txtSCause']."','".$_POST['txtSFName']."',
    '".$_POST['txtSCountry']."','".$_POST['txtSAddress']."','".$_POST['txtSCity']."','".$_POST['txtSPostalCode']."','".$_POST['textDonationSingle']."',NOW())");
  }
  
?>
    </div>  
</form>

<?php
  if (isset($_POST['btnMDonation'])) {
    $insertQuery = mysqli_query($conn, "INSERT INTO donation VALUES(null,'".$_POST['txtMAmount']."','".$_POST['txtMCurrency']."',
      '".$_POST['txtMCause']."','".$_POST['txtMFName']."','".$_POST['txtMCountry']."','".$_POST['txtMAddress']."','".$_POST['txtMCity']."','".$_POST['txtMPostalCode']."','".$_POST['txtDonationMonthly']."',NOW())");
  }
?>

    </div>
    
  </div>
  <div class="col-md-12" style="border-radius: 25px;background:white;corner-radius:5px;border: 2px solid red;    margin-left: -5px;margin-top: 74px;">
    <h3 style="text-align:center;"><b>Cause Details.</b></h3></br>
<?php
$ddtype=$_GET['donation_type'];
    $queryr="SELECT description FROM `causes` WHERE id='$ddtype'";
    $cmdr=mysqli_query($conn,$queryr);
    while($rowr=mysqli_fetch_row($cmdr)){
        print $rowr[0];
    }
?>


</div>
</div>


  <div class="container">
    <div class="row">
     
       












<form id="regForm" class="MegaDonation"  action="">
  
  <!-- One "tab" for each step in the form: -->

<h1  >Mega Donation </h1>

  <div class="tab"> 
    <!-- <p><input placeholder="E-mail..." oninput="this.className = ''" name="email"></p>
    <p><input placeholder="Phone..." oninput="this.className = ''" name="phone"></p> -->


          <h4  >Your Details :</h4>
                  
                  <input class="form-control" type="text"  placeholder="First Name"   name="textFName" value="" ><br>
                  <input class="form-control" type="text"  placeholder="Last Name"  name="textLName" value="" ><br>
                  <input class="form-control" type="Email" placeholder="Email"    name="txtEMail" value="" ><br>
                  <input class="form-control" type="Email" placeholder="Cell Number"  name="textCellNumber" value="" ><br>
  </div>



  <div class="tab"> 

<h4  >Your Address  :</h4>
                  <input class="form-control" type="text" placeholder="Address"     name="txtAddress" value="" ><br>
                  <input class="form-control" type="text" placeholder="Town or City"  name="txtCity" value="" ><br>
                  <input class="form-control" type="text" placeholder="Country"     name="txtCountry" value="" ><br>
                  <input class="form-control" type="text" placeholder="Postal Code"   name="txtPostaCode" value="" ><br>

  </div>

  <div class="tab"> 

<h4  >Donate For :</h4>
                  <select class="form-control">
                    
<?php
if(isset($_GET['donation_type'])){
 
  $donationId = $_GET['donation_type'];
  $query = mysqli_query($conn,"SELECT * FROM general_cause where gcause_id='".$donationId."'");
  while($row=mysqli_fetch_array($query)){
   
    ?>
    <option value="<?php echo $row['id'];?>"><?php echo $row['title'];?></option>
  <?php
  }
   
}else{


  $query1 = mysqli_query($conn,"SELECT * FROM subcauses ");
  while($row1=mysqli_fetch_row($query1)){
   
    ?>
    <option value="<?php echo $row1[0];?>"><?php echo $row1[1];?></option>
  <?php
  }
  $query="SELECT * FROM general_cause WHERE gcause_status='1'";
   $cmd=mysqli_query($conn,$query);
   while ($row=mysqli_fetch_row($cmd)) {
     print "<option value='$row[0]'>$row[1]</option>";
   }



}
?>
                  </select><br>


  </div>
  <div class="tab"> 

          <h4 >Card Details  :</h4>
                  <input class="form-control" type="text" placeholder="Issuer"        name="txtIssuer" value="" ><br>
                  <input class="form-control" type="text" placeholder="Credit Card Number"  name="txtCreditCard" value="" ><br>
                  <input class="form-control" type="text" placeholder="Secure Code"       name="txtSecureCode" value="" ><br>
                  <input class="form-control" type="text" placeholder="Expiry Month"      name="txtExpMonth" value="" ><br>
                  <input class="form-control" type="text" placeholder="Expiry Year"       name="txtExpYear" value="" ><br>  
                   

  </div>



  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>




  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
  </div>
</form>

 




 



    </div>
  </div>  

<style>
 .uii{
  padding:10px !important;
  min-height:200px!important;
  
 }
 .uii p{
  margin-top: 0px !important;
      min-height: 60px;
      text-align:center;
 }
</style>
<section class="news-update-section">
        <div class="container">
          <div class="Theme-title text-center">
            <h2 style="margin-top: -79px;">Your Donations</h2>
          </div> <!-- /.Theme-title -->
      <?php
      $offset=0;
      $query="SELECT * FROM general_cause where gcause_status='1'";
      $cmd=mysqli_query($conn,$query);
      while($row=mysqli_fetch_row($cmd)) {
        
      

      ?>
   <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="home-news-update-item" style="height:500px;">
                <div class="news-update-img"><img style="width:500px; height:200px;" src="generalcause/<?php echo $row[4].".".$row[5] ?>" alt="image"></div><!-- /.news-update-img -->
                <div class="news-update-text uii">
                  <!-- <span> <i class="fa fa-clock-o" aria-hidden="true"></i> March 4, 2017 1:10 pm</span> -->
                  <p style="    text-align: left;"><?php echo $row[1];?></p>
             <div style="text-align:center;">
                  <button type="button" class="btn btn-primary" style="background:#fd580b; width:200px;"><a style="color:white;" href='donationForm.php?donation_type=<?php echo $row[0]?>&causetype=generalcause'>Donate Now</a></button></div>
                </div> <!-- /.news-update-text -->
              </div> <!-- /.home-news-update-item -->
            </div> <!-- /.col -->

<?php } ?>
  </div> <!-- /.home-news-update-wrapper -->
        </div> <!-- /.container -->
      </section> <!-- /.news-update-section -->



</div>

<nav aria-label="Page navigation example" style="text-align:center;">
  <ul class="pagination">
    <?php if($offset!=0){
     print "<li class='page-item'><a class='' href='donationForm.php?offset=$offset'>Previous</a></li>";}?>
    <?php
    $total=0;
    $query="SELECT COUNT(*) FROM general_cause";
    $cmd=mysqli_query($conn,$query);
    while ($row=mysqli_fetch_row($cmd)) {
      $total=$row[0];
      
    }
?>
      <?php
      // $result=$total/6;
      // $result=floor($result);
      // if($total%6!=0){
      //   $result+=1;
      // }
      // for($b=1;$b<=$result;$b++){
      //     print "<li class='page-item'><a class='page-link' href='donationForm.php?offset=$b'>$b</a></li>";
      // }
      // $buff=$offset+2;
    ?>
    <?php
    // if($offset!=$result-1){
    //     print "<li class='page-item'><a class='page-link' href='donationForm.php?offset=$buff'>Next</a></li>";}
    ?>
  </ul>
</nav>

<script>
    function donate_amount(){
        //  alert(document.getElementById('uniqid').value);
        var uniqid=document.getElementById('uniqid').value;
       
        var donation_type=document.getElementById('txtSCause').value;
        var full_name=document.getElementById('txtSFName').value;
        
        var country=document.getElementById('txtSFCountry').value;
        var city=document.getElementById('txtSCity').value;
         var address=document.getElementById('txtSAddress').value;
        var postal_code=document.getElementById('txtSPostalCode').value;
        
        
        var params = 'full_name='+full_name+'&country='+country+'&city='+city+'&postal_code='+postal_code+'&donation='+donation_type+'&submit="btnSdonate"'+'&address='+address+'&unique_id='+uniqid;
         
          var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    console.log(this.responseText);
                    
                  if(this.responseText=='success'){
                        // document.paypal_form.submit();
                         document.getElementById('paypal_form').submit();
                  }
                 
                 
                }
              };
             
              xhttp.open('POST', 'ajax_donation_info.php', true);

            //Send the proper header information along with the request
             xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
             xhttp.send(params);
    }
   
    
    
    
     
   
    function donate_monthly_amount(){
        //  alert(document.getElementById('uniqid').value);
        var uniqid=document.getElementById('uniqid').value;
     
        var donation_type=document.getElementById('txtSCause').value;
        var gmail=document.getElementById('txtSgmail').value;
        var full_name=document.getElementById('txtSFName').value;
        
        var country=document.getElementById('txtSFCountry').value;
        var city=document.getElementById('txtSCity').value;
         var address=document.getElementById('txtSAddress').value;
        var postal_code=document.getElementById('txtSPostalCode').value;
        
        
        var params = 'full_name='+full_name+'&gmail='+gmail+'&country='+country+'&city='+city+'&postal_code='+postal_code+'&donation='+donation_type+'&submit="btnSdonates"'+'&address='+address+'&unique_id='+uniqid;
         
          var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    console.log(this.responseText);
                    
                  if(this.responseText=='success'){
                   
                    
                         document.getElementById('paypal_forms').submit();
                  }
                 
                 
                }
              };
             
              xhttp.open('POST', 'ajax_monthly.php', true);

            //Send the proper header information along with the request
             xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
             xhttp.send(params);
    }
   
    
    
    
        
</script>
<?php include('footer.php'); ?>
<?php
function showCurrency()
{
  $currency=array('Lek  ALL ','AFN   ؋','ARS  $','AWG  ƒ','AUD  $','AZN  ₼','BSD  $','BBD  $','BYN  Br','BZD  BZ$','BMD  $','BOB  $b','BWP  P','BGN  лв','BRL  R$','BND  $','KHR  ៛','CAD  $','KYD  $','CLP  $','CNY  ¥','COP  $','CRC  ₡','HRK  kn','CUP  ₱','CZK  Kč','DKK  kr','DOP  RD$','XCD  $','EGP  £','SVC  $','EUR  €','FKP  £','FJD  $','GHS  ¢','GIP  £','GTQ  Q','GGP  £','GYD  $','HNL  L','HKD  $','HUF  Ft','ISK  kr','INR  ₹','IDR  Rp','IRR  ﷼','IMP  £','ILS  ₪','JMD  J$','JPY  ¥','JEP  £','KZT  лв','KPW  ₩','KRW  ₩','KGS  лв','LAK  ₭','LBP  £','LRD  $','MKD  ден','MYR  RM','MUR  Rs','MXN  $','MNT  ₮','MZN  MT','NAD  $','NPR  ₨','ANG  ƒ','NZD  $','NIO  C$','NGN  ₦','NOK  kr','OMR  ﷼','PKR  ₨','PAB  B/.','PYG  Gs','PEN  S/.','PHP  ₱','PLN  zł','QAR  ﷼','RON  lei','RUB  ₽','SHP  £','SAR  ﷼','RSD  Дин','SCR  ₨','SGD  $','SBD  $','SOS  S','ZAR  R','LKR  Rs','SEK  kr','CHF  CHF','SRD  $','SYP  £','TWD  NT$','THB  ฿','TTD  TT$','TRY','TVD  $','UAH  ₴','GBP  £','USD  $','UYU  $U','UZS  лв','VEF  Bs','VND  ₫','YER  ﷼','ZWD  Z$','');
        
$size=sizeof($currency);
for($b=0;$b<$size;$b++){
  print "<option value='$currency[$b]'>$currency[$b]</option>";
}
}

?>