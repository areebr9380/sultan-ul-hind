<?php 
	include('header.php');
	
 ?>
	
	
		
		<div class="main-page-wrapper">

			<!-- Header _________________________________ -->
			
			<!-- Theme Main Banner ____________________________ -->
			<section>
				<div id="theme-main-banner">
				<?php 

					$query = mysqli_query($conn,"SELECT * FROM slider WHERE status = 1");
					while ($row = mysqli_fetch_assoc($query)) {
						$sliderTitle =$row['title'];
						$sliderImage =$row['image'].".".$row['extension'];
?>

			
					<div data-src="sliders/<?php echo $sliderImage;?>">
						<div class="camera_caption">
							<div class="container text-center">
							    <h1 class="wow fadeInUp animated" ><?php echo $sliderTitle;?></h1>
							    <h6 class="wow fadeInUp animated" >join today</h6>
								<a href="donationForm.php?donation_type=<?php echo $row['id'];?>&causetype=slider" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right">Donate Now</a>
							</div> <!-- /.container -->
						</div> <!-- /.camera_caption -->
					</div>
<?php				
}
				 ?>
			</div> <!-- /#theme-main-banner -->
			</section>

			<!-- Children Care List  _________________________________ -->
			<section class="Children-Care-list-margin">
				<div class="container">
					<div class="Children-Care-list">
						<div id="Children-Care-List-Slider" class="owl-carousel owl-theme">
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-handshake"></i>
									<h6><a href="#">Children’s Care</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="fa fa-eur"></i>
									<h6><a href="#">Donate</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation"></i>
									<h6><a href="#">Volunteer</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation-2"></i>
									<h6><a href="#">Food assistance</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
						</div> <!-- / #Children-Care-List-Slider -->
					</div> <!-- /.Children-Care-list -->
				</div> <!-- /.container -->
			</section> <!-- /.Children-Care-list-margin -->

			<!-- Banner Bottom Section _________________________________ -->
			<section class="banner-bottom-section">
				<div class="opact-div">
					<div class="container">
						<div class="row">
							<div class="col-md-9 col-xs-12">
								<div class="banner-bottom-section-text">
									<h3>Make a single or monthly donation today</h3>
									<p>Find out how you can help children affected by poverty, conflict and natural disasters through a one-off donation or a pledge of regular support.</p>
								</div> <!-- /.banner-bottom-section-text -->
							</div> <!-- /.col -->
							<div class="col-md-3 col-xs-12">
								<div class="banner-bottom-section-button clear-fix">
									<div><a href="#" class="hvr-bounce-to-right">Learn More !</a></div>
								</div> <!-- /.banner-bottom-section-button -->
							</div> <!-- /.col -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.opact-div -->
			</section> <!-- /.banner-bottom-section -->

			<!-- You Can Bring ____________________________ -->
			<section class="You-Can-Bring">
				<div class="You-Can-Bring-Title">
					<div class="container">
						<div class="row">
							<div class="col-lg-5 col-sm-6 col-xs-12">
								<h3>You can bring real hope by €</h3>
							</div> <!-- /.col -->
							<div class="col-lg-7 col-sm-6 col-xs-12">
								<p>Your donation powers our response to disasters, providing shelter, food, emotional support and other necessities to those affected</p>
							</div> <!-- /.col -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.You-Can-Bring-Title -->
				<div class="You-Can-Bring-Item-Wrapper">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<div class="Bring-Item text-center">
								<h3>Help our School Children</h3>
								<p>Be inspired! Help school going children through your donation and support</p>
								<span></span>
								<a href="#" class="a-comon hvr-bounce-to-right">Donate now !</a>
							</div> <!-- /.Bring-Item -->
						</div> <!-- /.col -->
						<div class="col-sm-6  col-xs-12">
							<div class="Bring-Item bring-item-bg-two text-center">
								<h3>2 EURO MEAL APPEAL FOR PAKISTAN</h3>
								<p>Your two euro donation can contribute to bring one time meal for a person seeking food assistance</p>
								<span></span>
								<a href="#" class="a-comon hvr-bounce-to-right">Donate Now !</a>
							</div> <!-- /.Bring-Item -->
						</div> <!-- /.col -->
					</div> <!-- /.row -->
				</div> <!-- /.You-Can-Bring-Item-Wrapper -->
			</section> <!-- /.You-Can-Bring -->

			<!-- Rcent Causes ____________________________ -->
			<section class="Rcent-Causes-Section">
				<div class="container">
					<div class="Theme-title text-center">
						<h2>Recent Causes</h2>
						<h6>We need you</h6>
					</div> <!-- /.Theme-title -->

					<div class="Rcent-Causes-Item-Wrapper">
						<div id="Rcent-Causes-Slider" class="owl-carousel owl-theme">
							
<?php 
	$cause = mysqli_query($conn,"SELECT * FROM causes where status = 1");
	while ($causeRow = mysqli_fetch_array($cause)) {
	    $country="";
	    if($causeRow['country']!=""){
	        $country="( ".$causeRow['country']." )";
	    }
		echo('									
						<div class="item">
							<div class="Causes-Item">
								<div class="Causes-Img"><img style="width:500px; height:200px;" src="causes/'.$causeRow['image'].".".$causeRow['ext'].'" alt=""></div> <!-- /.Causes-Img -->
									<div class="Causes-Text">
										<h3 style="min-height: 157px;">'.$causeRow['title'].'<br>'.$country.'</h3>
										
										<p style="min-height: 157px;">');$id=$causeRow['id'];
		$query="SELECT * FROM subcauses WHERE cause_id='$id'";
		$cmd=mysqli_query($conn,$query);
			while($row=mysqli_fetch_row($cmd))
			{
				print $row[1]."</br>";
			}


		print('</p>
										<a href="donationForm.php?donation_type='.$causeRow['id'].'&causetype=causes">Donate Now</a>
									</div> <!-- /.Causes-Text -->
								</div> <!-- /.Causes-Item -->
							</div> <!-- /.item -->
');
	}
?>							
							
						</div> <!-- /.row -->
						<a href="showCauses.php" class="hvr-float-shadow">Load more Causes</a>
					</div> <!-- /.Rcent-Causes-Item-Wrapper -->
				</div> <!-- /.container -->
			</section> <!-- /.Rcent-Causes-Section -->

			<!-- Company History _________________________________ -->
			<!-- <section class="company-history-section">
				<div class="company-history-shape-img-top"><img src="images/shape/shape-1.png" alt="shape-img"></div> --><!-- /.company-history-shape-img-top -->
				<!-- <div class="company-history-containt-opact">
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-xs-6 history-item-weight">
								<div class="clear-fix">
									<div class="history-item item-one">
										<div>
											<i class="fa fa-eur"></i>
											<p>Fundrising</p>
											<h2><span class="timer" data-from="0" data-to="1425" data-speed="2000" data-refresh-interval="5">0</span></h2>
										</div>
									</div> --> <!-- /.history-item -->
								<!-- </div> --> <!-- /.clear-fix -->
							<!-- </div> -->
							<!-- /.col -->
							<!-- <div class="col-lg-3 col-xs-6 history-item-weight">
								<div class="clear-fix">
									<div class="history-item item-two">
										<div>
											<i class="flaticon-group"></i>
											<p>Volunteer</p>
											<h2><span class="timer" data-from="0" data-to="1200" data-speed="2000" data-refresh-interval="5">0</span></h2>
										</div>
									</div> --> <!-- /.history-item -->
								<!-- </div> --> <!-- /.clear-fix -->
							<!-- </div> --> <!-- /.col -->
							<!-- <div class="col-lg-3 col-xs-6 history-item-weight">
								<div class="clear-fix">
									<div class="history-item item-three">
										<div>
											<i class=""></i>
											<p>Donator</p>
											<h2><span class="timer" data-from="0" data-to="201" data-speed="2000" data-refresh-interval="5">0</span></h2>
										</div>
									</div> --> <!-- /.history-item -->
								<!-- </div> --> <!-- /.clear-fix -->
							<!-- </div> --> <!-- /.col -->
							<!-- <div class="col-lg-3 col-xs-6 history-item-weight">
								<div class="clear-fix">
									<div class="history-item item-four">
										<div>
											<i class="fa fa-eur"></i>
											<p>Raised Funds</p>
											<h2><span class="timer" data-from="0" data-to="20" data-speed="2000" data-refresh-interval="5">0</span>M</h2>
										</div>
									</div> --> <!-- /.history-item -->
								<!-- </div> --> <!-- /.clear-fix -->
							<!-- </div> --> <!-- /.col -->
						<!-- </div> --> <!-- /.row -->
					<!-- </div> --> <!-- /.container -->
				<!-- </div> --> <!-- /.company-history-containt-opact -->
				<!-- <div class="company-history-shape-img-bottom"><img src="images/shape/shape-2.png" alt="shape-img"></div> --><!-- /.company-history-shape-img-bottom -->
			<!-- 	 --> <!-- /.company-history-section -->

			<!-- Upcoming Events ____________________________ -->
			<section class="Upcoming-Events">
				<div class="container">
					<div class="Theme-title text-center">
						<h2>Upcoming & Recent Events</h2>
						<h6>Events</h6>
					</div> <!-- /.Theme-title -->

					<ul class="Events-Item-Wrapper">
						<li class="clear-fix">
<?php 

	$event = mysqli_query($conn,"SELECT * FROM event WHERE status = 1 LIMIT 1");
	while ($rowEvent = mysqli_fetch_array($event)) {
		$date = $rowEvent['date'];
		$orderdate = explode('-', $date);
		$year 		= $orderdate[0];
		$month 		= $orderdate[1];
		$day  		= $orderdate[2];

		$monthQuery = mysqli_query($conn,"SELECT * FROM month WHERE id ='$month'");
		while ($rowMonth = mysqli_fetch_array($monthQuery)) {
			$monthName = $rowMonth['monthName'];
		

		echo('							
							<div class="events-img"><img src="events/'.$rowEvent['image'].".".$rowEvent['extension'].'" alt="image"></div><!-- /.events-img -->
							<div class="day">
								<h2>'.$day.'</h2>
								<h6>'.$monthName.'<br>'.$year.'</h6>
							</div> <!-- /.day -->
							<div class="events-text">
								<h4>'.$rowEvent['title'].'</h4>
								<p><i>'.$day."-".$monthName."-".$year.', '.$rowEvent['start'].' - '.$day."-".$monthName."-".$year.', '.$rowEvent['end'].'</i><br>'.$rowEvent['venue'].'</p>
							</div> <!-- /.events-text -->
							<a href="events-details.php?eventId='.$rowEvent['id'].'"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
');
	}
}
?>							
						</li> <!-- /.clear-fix -->
					</ul> <!-- /.Events-Item-Wrapper -->
					<div class="row">
						<div class="col-md-4 col-xs-12">
							<div class="official-charity charity-one-bg-color">
								<h4>Run it forward official charity</h4>
								<p>Every day we bring real hope to millions of children in the world's hardest places as a sign of God's unconditional love </p>
							</div> <!-- /.official-charity -->
						</div> <!-- /.col -->
						<div class="col-md-4 col-xs-12">
							<div class="official-charity">
								<h4>Blankets and other necessities for homeless</h4>
								<p>Every day we bring real hope to millions of children in the world's hardest places as a sign of God's unconditional love</p>
							</div> <!-- /.official-charity -->
						</div>
						<div class="col-md-4 col-xs-12">
							<div class="official-charity charity-one-bg-color">
								<h4>Christmas presents for homeless</h4>
								<p>Every day we bring real hope to millions of children in the world's hardest places as a sign of God's unconditional love</p>
							</div> <!-- /.official-charity -->
						</div> <!-- /.col -->

					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</section> <!-- /.Upcoming-Events -->

			<!-- testimonial section _________________________________ -->
			<section class="testimonial-section">
				<div class="testimonial-shape-img"><img src="images/shape/shape-3.png" alt="shape-img"></div><!-- /.shape-img -->
				<div class="testimonial-opact">
					<div class="testimonial-containt">
						<div class="container">
							<div class="testimonial-shape-border"><i class="flaticon-right-quotation-sign"></i></div>

							<div id="client-carousel" class="carousel slide" data-ride="carousel" data-interval="2500">
								<!-- Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#client-carousel" data-slide-to="0"></li>
									<li data-target="#client-carousel" data-slide-to="1" class="active"></li>
									<li data-target="#client-carousel" data-slide-to="2"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
								    <div class="item">
								     	<p>Noblest of character is possessed by one who is pious in poverty, content in hunger, cheerful in grief and friendly in hostility.</p>
								     	<span>Hazrat Khawaja Moin Uddin Hassan Chishti (R.A)</span>
								     	<img src="images/logo/logo.png" alt="logo">
								    </div> <!-- /.item -->
								    <div class="item active">
								     	<p>Noblest of character is possessed by one who is pious in poverty, content in hunger, cheerful in grief and friendly in hostility.</p>
								     	<span>Hazrat Khawaja Moin Uddin Hassan Chishti (R.A)</span>
								     	<img src="images/logo/logo.png" alt="logo">
								    </div> <!-- /.item -->
								    <div class="item">
								    	<p>Noblest of character is possessed by one who is pious in poverty, content in hunger, cheerful in grief and friendly in hostility.</p>
								     	<span>Hazrat Khawaja Moin Uddin Hassan Chishti (R.A)</span>
								     	<img src="images/logo/logo.png" alt="image">
								    </div> <!-- /.item -->
								</div> <!-- /.carousel-inner -->
							</div> <!-- Wrapper for bootstrap slides -->
						</div> <!-- /.container -->
					</div> <!-- /.testimonial-containt -->
				</div> <!-- /.testimonial-opact -->
			</section> <!-- /.testimonial-section -->

			<!-- News Update _________________________________ -->
			<section class="news-update-section">
				<div class="container">
					<div class="Theme-title text-center">
						<h2>News Update</h2>
						<h6>Latest News</h6>
					</div> <!-- /.Theme-title -->

					<div class="row home-news-update-wrapper">
					<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="home-news-update-item">
								<div class="news-update-img"><img src="images/Homeless.jpg"  alt="image"></div><!-- /.news-update-img -->
								<div class="news-update-text" style="height: 500px;">
									<span> <i class="fa fa-clock-o" aria-hidden="true"></i> November 7, 2018</span>
									<p>According to the figures released by the Department</p>
									<a href="blog-v1.php"><i class="fa fa-arrow-right" aria-hidden="true"></i> Learn more</a>
								</div> <!-- /.news-update-text -->
							</div> <!-- /.home-news-update-item -->
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="home-news-update-item">
								<div class="news-update-img"><img src="images/home/img-5.jpg"  alt="image"></div><!-- /.news-update-img -->
								<div class="news-update-text" style="height: 500px;">
									<span> <i class="fa fa-clock-o" aria-hidden="true"></i> January 24, 2019</span>
									<p>More than 145,000 Rohingya refugee children living in camps in south-east Bangladesh are now attending UNICEF-supported learning centres, as a new school year begins</p>
									<a href="blog-v1.php"><i class="fa fa-arrow-right" aria-hidden="true"></i> Learn more</a>
								</div> <!-- /.news-update-text -->
							</div> <!-- /.home-news-update-item -->
						</div> <!-- /.col -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="home-news-update-item">
								<div class="news-update-img"><img src="images/home/img-6.jpg" alt="image"></div><!-- /.news-update-img -->
								<div class="news-update-text" style="height: 500px;">
									<span> <i class="fa fa-clock-o" aria-hidden="true"></i> January , 2019</span>
									<p>According to UNICEF every year, around 3 million children die due to undernutrition. For millions more, chronic malnutrition may result in stunting – an irreversible condition that literally stunts the physical and cognitive growth of children</p>
									<a href="blog-v1.php"><i class="fa fa-arrow-right" aria-hidden="true"></i> Learn more</a>
								</div> <!-- /.news-update-text -->
							</div> <!-- /.home-news-update-item -->
						</div> <!-- /.col -->

					</div> <!-- /.home-news-update-wrapper -->
				</div> <!-- /.container -->
			</section> <!-- /.news-update-section -->

<?php include('footer.php') ?>			

	
