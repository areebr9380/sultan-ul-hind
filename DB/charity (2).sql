-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2019 at 01:29 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `charity`
--

-- --------------------------------------------------------

--
-- Table structure for table `causes`
--

CREATE TABLE `causes` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `country` text NOT NULL,
  `image` int(100) NOT NULL,
  `ext` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `causes`
--

INSERT INTO `causes` (`id`, `title`, `country`, `image`, `ext`, `description`, `status`) VALUES
(1, 'Support a child', 'Karachi Pakistan', 1, 'jpg', 'Support needy children', 1),
(2, 'Health Assistance Program', 'Ireland, Pakistan, Somalia, Syria, Yemen, Others', 2, 'jpg', 'Health Assistance Program Description', 1),
(3, 'Assistance For Homeless', 'Ireland, Pakistan, Somalia, Others', 3, 'jpg', 'Homeless People', 1);

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE `donation` (
  `id` int(255) NOT NULL,
  `cause` varchar(250) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `country` text NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `uniqid` varchar(250) NOT NULL,
  `status` int(250) NOT NULL DEFAULT '0',
  `amount` varchar(250) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donation`
--

INSERT INTO `donation` (`id`, `cause`, `full_name`, `email`, `country`, `address`, `city`, `postal_code`, `uniqid`, `status`, `amount`, `date_time`) VALUES
(20, '', 'dfsg', '', 'Afghanistan', 'dfg', 'dfg', '4534', '5f48a2ad3c0f295da19d8a24d82ae826,19703', 0, '', '2019-01-18 08:14:25'),
(21, '', 'Arsalan', '', 'Afghanistan', 'sad', 'asda', '24', '8e76457c66dce5c5826494760eeb600c,26840', 0, '', '2019-01-18 09:41:14'),
(25, '', '', '', 'Afghanistan', '', '', '', 'ce8a4a5f86725a5cdf190c6330543231,503545', 0, '', '2019-01-18 09:41:57'),
(31, '', '', '', 'Afghanistan', '', '', '', '7ec99d98fdfe4946950389cd87a28cd7,363267', 0, '', '2019-01-18 09:45:17'),
(32, '', 'weeq', '', 'Afghanistan', 'qewq', 'wqeq', 'qweqw', '039270e83c4e86cfdc400feb0f503d17,563841', 1, '45.00', '2019-01-18 09:52:52'),
(33, '', 'ADS', '', 'Afghanistan', 'SAD', 'SAD', 'DSA', '020630b451bf5aa8e7763c1831beed24,14211', 0, '', '2019-01-18 12:04:20'),
(34, '', '', '', '1', '', 'Afghanistan', '', '', 0, 'monthly', '2019-01-19 05:27:54'),
(36, '', '', '', 'Afghanistan', '', '', '', 'f63b13afe1eedcd2411eddd2a41ee4a4,967993', 1, '223.00', '2019-01-19 05:32:45'),
(37, '', '', '', 'Afghanistan', '', '', '', '86431774a6924e8bdaefc8cddd8935c4,530113', 0, '', '2019-01-19 05:45:23'),
(38, '', '', 'gmail', 'Afghanistan', '', '', '', '260182b82a027aaa36c15622db75c796,199989', 0, '', '2019-01-19 06:04:02'),
(41, '', '', 'rtytry@gmail.com', 'Afghanistan', '', '', '', 'b6d056bdaa2cd26f320bbca229705136,93040', 0, '', '2019-01-19 06:06:36'),
(43, '', '', 'arsalan@ptnest.com', 'Afghanistan', '', '', '', 'bfa1b61f8b82b7fc1b8b7b81db8671d2,406413', 0, '', '2019-01-19 07:00:10'),
(46, '', '', 'dsfa', 'Afghanistan', '', '', '', '445d9c61f74c464c93c14398e9600ce7,12895', 0, '', '2019-01-19 07:03:09'),
(52, '', '', 'asdasd', 'Afghanistan', '', '', '', 'f9c055af43c3cdbe4c84e59512715757,789213', 0, '', '2019-01-19 07:10:28'),
(54, '', 'ADS', '', 'Afghanistan', 'SAD', 'SAD', 'DSA', '14e8d085cdb543cf03878c5d722e813b,526173', 0, '', '2019-01-19 07:14:59'),
(57, '', 'dsa', 'arsa@gmail.com', 'Algeria', '', '', '', 'f76e0091ad5fba491e6548a915662b57,659976', 0, '', '2019-01-19 07:19:44'),
(58, '', '', 'aaaaaars@gmail.com', 'Afghanistan', '', '', '', 'e1ae4110eab508e8bfe904db4462826b,24705', 0, '', '2019-01-19 07:25:24'),
(59, '', '', '', 'Afghanistan', '', '', '', '9b7b9c996b29b695f514d76568d1e5bc,837227', 0, '', '2019-01-19 07:26:32'),
(60, '', '', '', 'Afghanistan', '', '', '', '5eb1fc28f615f70a3a602c0fdabfd80a,485652', 0, '', '2019-01-19 07:34:07'),
(62, '', 'ADS', '', 'Afghanistan', '', '', '', '583da3c63e171a9c448dc71a8c371228,293612', 0, '', '2019-01-19 07:35:45'),
(63, '', '', 'dsaas', 'Afghanistan', '', '', '', '839340566e93258d6514fe3991e0cc54,493569', 0, '', '2019-01-19 07:36:14'),
(64, '', 'sd', 'asd@gmail.com', 'Afghanistan', '', '', '', 'ff40eb4abbd5fe82b5a5af6eff10334b,443913', 0, '', '2019-01-19 07:37:51'),
(65, '', '', 'asd@gmail.com', 'Afghanistan', '', '', '', '3edd984c3baa7a58cdae399c5f395a82,238700', 0, '', '2019-01-19 09:09:48'),
(66, '', '', 'arsalan@ptnest.com', 'Afghanistan', '', '', '', 'c43cd01cbdf29984be6ee6dfcb02799a,711322', 1, '500.00', '2019-01-21 10:55:51'),
(67, '', 'asd', '', 'Albania', 'asd', '', '', 'ac0d63461d74fae79142b25fa09f25d9,690964', 1, '343.00', '2019-01-21 11:11:24'),
(68, '', 'asd', '', 'Afghanistan', '', '', '', 'b26d9bab29965d7feecdbf388a04e523,321375', 1, '67.00', '2019-01-21 11:18:36'),
(69, '', '', '', 'Afghanistan', '', '', '', 'eff3888cdb53353434a21547bff6d180,631347', 1, '45.00', '2019-01-21 11:28:59'),
(70, '', 'dsf', '', 'Azerbaijan', 'dsf', 'dsf', 'sdfds', '37e01b56f3a8ab7febd8dd9a8ac48b72,837897', 1, '34.00', '2019-01-21 11:40:50'),
(71, '', 'asd', '', 'Afghanistan', 'asdas', 'dasd', 'asd', '09125d395fe57e0213ee5f8eb8f7a6ee,318465', 0, '', '2019-01-21 11:57:47'),
(72, '', 'sdad', '', 'Afghanistan', '', '', '', 'd04b9abc537e4e2fd7446c4e2a5d32eb,247198', 0, '', '2019-01-21 12:17:38'),
(73, '4', 'sad', '', 'Albania', 'sdfa', 'dasf', '2343', '5a2c7b0039f93b373dc5814531f1fd45,771633', 0, '', '2019-01-21 12:22:46'),
(74, '2', 'asd', '', 'Afghanistan', '32423', 'das', 'asd', 'fc952867ea47e43a2fc420dc6de77ed8,434484', 0, '', '2019-01-22 09:52:21'),
(75, '2', 'qwe', '', 'Afghanistan', 'qwe', 'qwe', 'qwe', '8dd57bc344e41c6a02060da7ea4ab558,609240', 1, '234.00', '2019-01-22 09:59:14'),
(76, '2', 'faraz', '', 'Ireland', '14 bremore pastures', 'Balbriggan', 'K32AK75', '0199c0c484b4338ecb24bf970eff7eff,63756', 0, '', '2019-01-22 13:44:59'),
(87, '1', 'faraz', '', 'Ireland', '14 bremore pastures', 'Balbriggan', 'K32AK75', '2753fe9aa1692adf75a63673769a7702,957552', 0, '', '2019-01-22 13:49:17'),
(88, '3', 'Junaid', '', 'Pakistan', 'Testing', 'Karachi', '2636', 'e5a7bcbf97c8c4f97ed891d5bb8e2de4,319420', 1, '20.00', '2019-01-22 14:13:13'),
(89, '8', 'faraz khan', '', 'Ireland', '14 bremore pasturecrescent', 'balbriggan county dublin', 'K32AK75', '75b5e1fcfd5638da9ca49e7482fc4f57,473874', 0, '', '2019-01-22 17:46:27'),
(90, '1', 'faraz khan', '', 'Ireland', '14 Bremore Pasture Crescent', 'balbriggan county dublin', 'K32AK65', '136ed472af8168daaeffc9d50c6aa9d9,962619', 0, '', '2019-01-22 17:51:15');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `designation` varchar(255) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `password` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `emp_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emp_type`
--

CREATE TABLE `emp_type` (
  `id` int(11) NOT NULL,
  `emp_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_type`
--

INSERT INTO `emp_type` (`id`, `emp_type`, `status`) VALUES
(1, 'Admin', 1),
(2, 'Employee', 1);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` int(100) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `venue` text NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `image`, `extension`, `date`, `start`, `end`, `venue`, `description`, `location`, `status`) VALUES
(1, 'Charities Regulator''s event on Charity Trustees'' Week - Academy Plaza Hotel, Dublin', 1, 'jpg', '2018-11-13', '20:00:00', '22:00:00', 'Charities Regulator''s event on Charity Trustees'' Week - Academy Plaza Hotel, Dublin', 'Charities Regulator''s event on Charity Trustees'' Week - Academy Plaza Hotel, Dublin', '', 1),
(2, 'Food Assistance for Homeless - Dubline Ireland', 1, 'jpg', '2018-12-19', '20:00:00', '22:00:00', 'Dubline Ireland', 'Food Assistance for Homeless - Dubline Ireland', '', 1),
(3, 'Christmas Presents for Homeless - Dubline Ireland', 5, 'jpg', '2018-12-25', '12:59:00', '00:59:00', 'Dubline Ireland', 'Christmas Presents for Homeless - Dubline Ireland', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3620.479093820411!2d67.02317741535309!3d24.84748158405996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb33ddfa27f7849%3A0xc2fbc4a56857f36a!2sPearl+Continental+Hotel!5e0!3m2!1sen!2s!4v1543068648995" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `general_cause`
--

CREATE TABLE `general_cause` (
  `gcause_id` int(11) NOT NULL,
  `gcause_title` varchar(60) NOT NULL,
  `gcause_description` varchar(500) NOT NULL,
  `gcause_country` varchar(50) NOT NULL,
  `gcause_img` int(11) NOT NULL,
  `gcause_img_ext` varchar(50) NOT NULL,
  `gcause_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_cause`
--

INSERT INTO `general_cause` (`gcause_id`, `gcause_title`, `gcause_description`, `gcause_country`, `gcause_img`, `gcause_img_ext`, `gcause_status`) VALUES
(3, 'Dig A Well', 'Dig a well for water', 'Pakistan', 3, 'jpg', 1),
(2, 'Food Packs for Pakistan', 'Food Packs for Pakistani needy people', 'Pakistan', 2, 'jpg', 1),
(4, 'Zakat', 'Zakat for needy people', 'Pakistan', 4, 'jpg', 1),
(5, 'Land for Graveyard Pakistan', 'land for graves', 'Pakistan', 5, 'jpg', 1),
(6, 'Support for homeless people Ireland', 'Ireland', 'Ireland', 6, 'jpg', 1),
(7, 'Food Packs For Other Countries', 'Food Packs For Ireland', 'Ireland', 7, 'jpg', 1),
(8, '2 â‚¬ Meal for Pakistan', 'Meal for Pakistab', 'Pakistan', 8, 'jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `id` int(11) NOT NULL,
  `monthName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`id`, `monthName`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `issuer` int(11) NOT NULL,
  `card_number` int(11) NOT NULL,
  `secure_code` int(11) NOT NULL,
  `expiry_month` int(11) NOT NULL,
  `expiry_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `role_name`) VALUES
(1, 'admin'),
(2, 'employee');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `image` int(100) NOT NULL,
  `extension` varchar(225) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `image`, `extension`, `status`) VALUES
(1, 'Food Assistance', 1, 'png', 1),
(2, 'Land for Graveyard ', 2, 'png', 1),
(3, 'Homeless People', 3, 'png', 1),
(4, 'Child Support', 4, 'jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subcauses`
--

CREATE TABLE `subcauses` (
  `subcause_id` int(11) NOT NULL,
  `subcause_title` varchar(50) NOT NULL,
  `cause_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcauses`
--

INSERT INTO `subcauses` (`subcause_id`, `subcause_title`, `cause_id`) VALUES
(1, 'Support a child education', 1),
(2, 'Children necessities', 1),
(3, 'Children Health', 1),
(4, 'Malnutrition', 2),
(5, 'Provision of Medicines', 2),
(6, 'Support for treatment of diseases', 2),
(7, 'Shelter', 3),
(8, 'Blankets', 3),
(9, 'Households necessities', 3);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cell_number` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `post_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(225) NOT NULL,
  `cell_number` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(10) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `f_name`, `lname`, `email`, `cell_number`, `password`, `address`, `city`, `country`, `role_id`, `status`) VALUES
(1, 'ptnest', 'org', 'ptnest@ptnest.com', '0333-2221111', 'ptnest123', 'Karachi Pakistan', 'Karachi', 'Pakistan', 1, 1),
(2, 'Hassan', 'Khan', 'hk@gmail.com', '030212144', 'asd-123', 'Johar', 'Karachi', 'Paksitan', 2, 1),
(3, 'Ali', 'Khan', 'ali@gmail.com', '0300-21365478', '123456789', 'Nazimabad', 'Karachi', 'Pakistan', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `causes`
--
ALTER TABLE `causes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donation`
--
ALTER TABLE `donation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniqid` (`uniqid`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_type`
--
ALTER TABLE `emp_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_cause`
--
ALTER TABLE `general_cause`
  ADD PRIMARY KEY (`gcause_id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcauses`
--
ALTER TABLE `subcauses`
  ADD PRIMARY KEY (`subcause_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `causes`
--
ALTER TABLE `causes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `donation`
--
ALTER TABLE `donation`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `emp_type`
--
ALTER TABLE `emp_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `general_cause`
--
ALTER TABLE `general_cause`
  MODIFY `gcause_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `month`
--
ALTER TABLE `month`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `subcauses`
--
ALTER TABLE `subcauses`
  MODIFY `subcause_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
