<!-- them-main-footer-section _________________________________ -->
			<footer class="them-main-footer-section">
				<div class="main-footer-top-section">
					<div class="container">
						<div class="footer-top-item">

							<h3>Your donations may bring life into the lives of many deservings</h3>
							<div class="Subscribe-footer-form">
								<p>Latest news delivered right to your inbox!</p>
								<form action="#">
									<input type="text">
									<button class="tran3s hvr-bounce-to-right">Subscribe</button>
								</form>
							</div> <!-- /.Subscribe-footer-form -->
						</div> <!-- /.footer-top-item -->
					</div> <!-- /.container -->
				</div> <!-- /.main-footer-top-section -->

				<div class="them-main-footer-containt-item">
					<div class="container">
						<div class="row">
							<div class="col-md-5 col-xs-12">
								<div class="footer-containt-item-text-and-logo">
									<div class="footer-logo"><a href="index.php"><img src="images/logo/logo-footer.png" alt="logo"></a></div>
									<p style="margin-top: -32px; line-height: 23px !important; text-align: justify;">Sultan ul Hind Trust is a private charitable trust attributed to Great Sufi Saint Hazrat Khwaja Moin uddin Hassan Chishti popularly known as Hazrat Khawaja Gharib Nawaz (R.A). Trust is registered in charity regulator in Ireland having Registered Charity Number <strong style="color:white;">(RCN): 20200021</strong>.  Sultan Ul Hind Trust has been registered in Pakistan Since 2006. We have been serving people in term of food,shelter,medicine,health and others.</p>
								</div> <!-- /.footer-containt-item-text-and-logo -->
							</div> <!-- /.col -->
							<div class="col-md-7 col-xs-12">
								<div class="row">
									<div class="col-xs-4 footer-containt-width">
										<div class="footer-containt-item">
											<ul>
												<li><a href="#">Donate</a></li>
												<li><a href="showEvents.php">Events</a></li>
												<!-- <li><a href="#">Interpreter Service</a></li> -->
												<li><a href="#">Archives</a></li>
												<li><a href="#">Apply as Volunteer</a></li>
												<li><a href="#">Privacy</a></li>
											</ul>
										</div> <!-- /.footer-containt-item -->
									</div> <!-- /.col -->
									<div class="col-xs-4 footer-containt-width">
										<div class="footer-containt-item">
											<ul>
												<li><a href="about-us.php">Who we are</a></li>
												<li><a href="shop.php">Shop</a></li>
												<li><a href="#">News</a></li>
												<li><a href="contact-us.php">Contact Us</a></li>
											</ul>
										</div> <!-- /.footer-containt-item -->
									</div> <!-- /.col -->
									<div class="col-xs-4 footer-containt-width">
										<div class="footer-containt-item">
											<ul>
												
												<!-- <li><a href="#">404 Page</a></li> -->
												<!-- <li><a href="#">Coming Soon</a></li> -->
												<li><a href="about-us.php">About us</a></li>
												
											</ul>
										</div> <!-- /.footer-containt-item -->
									</div> <!-- /.col -->
								</div> <!-- /.row -->
							</div> <!-- /.col -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.them-main-footer-containt-item -->

				<div class="main-footer-bottom-section">
					<div class="container">
						<div class="clear-fix">
							<ul class="footer-bottom-left-said">
								<li><span> <a href="index.php"></a> ©
								<?php
								print $yrr=date("Y");
								
								?>
								,</span></li>
								<li><span><a href="index.php">Sultan ul Hind Trust</a> All Rights Reserved. Powered By <a href="http://ptnest.com/">PTNEST</a> </span></li>
							</ul> <!-- /.footer-bottom-left-said -->
							<ul class="footer-bottom-right-said">
								<li><a href="https://www.facebook.com/Sultan-UL-Hind-Trust-Ireland-249013369354201/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<!--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
								<!--<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>-->
								<li><a href="https://www.gmail.com"><i class="fa fa-google" aria-hidden="true"></i></a></li>
								<!--<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>-->
							</ul> <!-- /.footer-bottom-right-said -->
						</div> <!-- /.clear-fix -->
					</div> <!-- /.container -->
				</div> <!-- /.main-footer-bottom-section -->
			</footer> <!-- /.them-main-footer-section -->
			<!--  PAGE===END  _________________________________ -->
		</div> <!-- /.main-page-wrapper -->
		

		<!-- Scroll Top Button -->
		<button class="scroll-top tran7s p-color-bg">
			<i class="fa fa-angle-up" aria-hidden="true"></i>
		</button>

		<!-- pre loader  -->
	 	<div id="loader-wrapper">
			<div id="loader"></div>
		</div>
	


		<!-- js file -->
		<!-- Main js file/jquery -->
		<script src="vendor/jquery-2.2.3.min.js"></script>
		<!-- bootstrap-select.min.js -->
		<script src="vendor/bootstrap-select-1.10.0/dist/js/bootstrap-select.min.js"></script>
		<!-- bootstrap js -->
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<!-- camera js -->
		<script src="vendor/Camera-master/scripts/camera.min.js"></script>
		<script src="vendor/Camera-master/scripts/jquery.easing.1.3.js"></script>
		<!-- Owl carousel -->
		<script src="vendor/OwlCarousel2/dist/owl.carousel.min.js"></script>
		<!-- appear & countTo -->
		<script src="vendor/jquery.appear.js"></script>
		<script src="vendor/jquery.countTo.js"></script>
		<!-- fancybox -->
		<script src="vendor/fancybox/dist/jquery.fancybox.min.js"></script> <!-- video -->
		<!--   <script src="vendor/fancybox/jquery.fancybox.pack.js"></script>   img -->
		<!-- Gallery - isotop -->
		<script type="text/javascript" src="vendor/isotope.pkgd.min.js"></script>
		<!-- WOW js -->
		<script type="text/javascript" src="vendor/WOW-master/dist/wow.min.js"></script>
		<!-- Circle Progress -->
		<script type="text/javascript" src="vendor/circle-progress.js"></script>
		<!-- Style js -->
		<script src="js/custom.js"></script>

</body>
</html>
<style>
	
	.MegaDonation,.MonthlyDonation{
		display: none;

	}

</style>
<!-- <script>
	
$(document).ready(function(){



    $("#donationtype").change(function(){
        // alert(this.value);
        var valuebyselect = this.value;
        if (valuebyselect == 'mega_donation') {
        	$('.MonthlyDonation').fadeOut(100);
        		$('.MegaDonation').fadeIn(100);
        }else if(valuebyselect == 'monthly_donation'){
        	$('.MegaDonation').fadeOut(100);
        	$('.MonthlyDonation').fadeIn(100);
        }else if(valuebyselect == 'please select'){
		$('.MegaDonation').fadeOut(100);
		$('.MonthlyDonation').fadeOut(100);

        }

      
    });
});

</script> -->
<script>

$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
});
   
 
</script>
