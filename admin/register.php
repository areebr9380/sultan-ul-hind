
<?php
    session_start();
    error_reporting(0);
    if(!isset($_SESSION['uid']))
    {
        print "<script>window.open('login.php','_self');</script>";
    }
    else{
        if($_SESSION['utype']!=1){
            print "<script>window.open('login.php','_self');</script>";
        }   
    }
?>

<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

<script>
    $(document).ready(function(){
        $("#sub").click(function(){
            var password=$("#password").val();
            var cpassword=$("#cpassword").val();
            
            if(cpassword!=password){
                alert("Passwords Do not match");
                event.preventDefault();
            }

        });
    });
</script>




<?php
include "../config.php";
    if(isset($_POST['register'])){
        $fname=$_POST['fname'];
        $lname=$_POST['lname'];
        $email=$_POST['email'];
        $pwd=$_POST['pwd'];
        $role=$_POST['role'];
        $cell_no=$_POST['cell_no'];
        $address=$_POST['address'];
        $city=$_POST['city'];
        $country=$_POST['country'];
        $query="INSERT INTO `users` (`users_id`, `f_name`, `lname`, `email`, `cell_number`, `password`, `address`, `city`, `country`, `role_id`, `status`) VALUES (NULL, '$fname', '$lname', '$email', '$cell_no', '$pwd', '$address', '$city', '$country', '$role', '1');";
        $cmd=mysqli_query($conn,$query);
        print "<script>alert('User Registered Successfully');</script>";
        print "<script>window.open('register.php','_self');</script>";
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Register</title>
    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Modernize Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta Tags -->

    <!-- Style-sheets -->
    <!-- Bootstrap Css -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Bootstrap Css -->
    <!-- Common Css -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!--// Common Css -->
    <!-- Fontawesome Css -->
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <!--// Fontawesome Css -->
    <!--// Style-sheets -->

    <!--web-fonts-->
    <link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!--//web-fonts-->
</head>

<body>
    <div class="bg-page py-5">
        <div class="container">
            <!-- main-heading -->
            <h2 class="main-title-w3layouts mb-2 text-center text-white">Register</h2>
            <!--// main-heading -->
            <div class="form-body-w3-agile text-center w-lg-50 w-sm-75 w-100 mx-auto mt-5">
                <form action="#" method="post">
                    <div class="form-group">
                        <label>User First Name </label>
                        <input type="text" class="form-control" name="fname" placeholder="Enter First Name" required="">
                    </div>
                    <div class="form-group">
                        <label>User Last Name</label>
                        <input type="text" class="form-control" placeholder="Enter username" name="lname" required="">
                    </div>
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter email" required="">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" id="password" name="pwd" placeholder="Password" required="">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" required="">
                    </div>
                    <div class="form-group">
                        <label>User's Role</label>
                        <select required="" name="role" class="form-control" style="height:40px;text-align:center;">
                        <option value="">Select User's Role</option>
                        <?php
                        $query="SELECT * FROM role";
                        $cmd=mysqli_query($conn,$query);
                        while ($row=mysqli_fetch_row($cmd)) {
                            print "<option value='$row[0]'>$row[1]</option>";
                        }
                        ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Cell Number</label>
                        <input type="text" class="form-control" name="cell_no" placeholder="Enter Cell No." required="">
                    </div><div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" name="address" placeholder="Enter username" required="">
                    </div><div class="form-group">
                        <label>City</label>
                        <input type="text" class="form-control" name="city" placeholder="Enter City" required="">
                    </div><div class="form-group">
                        <label>Country</label>
                        <input type="text" class="form-control" name="country" placeholder="Enter Country" required="">
                    </div>
                    
                    <button type="submit" class="btn btn-primary error-w3l-btn mt-sm-5 mt-3 px-4" name="register" id="sub">Submit</button>
                </form>
                <p class="paragraph-agileits-w3layouts mt-4">Already have account?
                    <a href="login.php">Login</a>
                </p>
                <h1 class="paragraph-agileits-w3layouts mt-2">
                    <a href="index.php">Back to Home</a>
                </h1>
            </div>

            <!-- Copyright -->
            <div class="copyright-w3layouts py-xl-3 py-2 mt-xl-5 mt-4 text-center">
                <p>© copyright <?php  echo date("Y") ?>. All Rights Reserved | Design by
                    <a href="//ptnest.com"> PTNest Solutions  </a>
                </p>
            </div>
            <!--// Copyright -->
        </div>
    </div>


    <!-- Required common Js -->
    <script src='js/jquery-2.2.3.min.js'></script>
    <!-- //Required common Js -->

    <!-- Js for bootstrap working-->
    <script src="js/bootstrap.min.js"></script>
    <!-- //Js for bootstrap working -->

</body>

</html>