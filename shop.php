<?php include('header.php'); ?>

			<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-two">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Shop</h1>
								<p>online shop for charity</p>
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><span>-</span></li>
									<li><a href="shop.php">Shop</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Need Our Help</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>

			<!-- Shop ____________________________ -->
			<section class="Shop-Pages">
				<div class="container">
					<div class="row">
						<div class="col-lg-9 col-md-8 col-xs-12 float-right">
							<div class="Shop-Item-Wrapper">
								<div class="row">
									<div class="col-lg-4 col-xs-6 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-1.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
									<div class="col-lg-4 col-xs-6 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-2.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
									<div class="col-lg-4 col-xs-6 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-3.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
									<div class="col-lg-4 col-xs-6 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-4.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
									<div class="col-lg-4 col-xs-6 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-5.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
									<div class="col-lg-4 col-xs-6 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-6.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
									<div class="col-lg-4 col-xs-6 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-7.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
									<div class="col-lg-4 col-xs-6 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-8.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
									<div class="col-lg-4 col-xs-12 Shop-Item-Width">
										<div class="Shop-Item">
											<div class="Shop-Img">
												<img src="images/shop/img-9.jpg" alt="image">
											</div> <!-- /.Shop-Img -->
											<div class="Shop-Cart">
												<h6><a href="shop-details.html"></a></h6>
												<span>€</span>
												<a href="#" class="hvr-float-shadow">Add To Cart</a>
											</div> <!-- /.Shop-Cart -->
										</div> <!-- /.Shop-Item -->
									</div> <!-- /.col -->
								</div> <!-- /.row -->
								<ul class="shop-next-and-pivias text-center">
									<li><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>
								</ul> <!-- /.shop-next-and-pivias -->
							</div> <!-- /.Shop-Item-Wrapper -->
						</div> <!-- /.col -->

						<div class="col-lg-3 col-md-4 col-xs-12">
							<!-- ================ Shop Side Bar ================== -->
							<div class="Shop-Side-Bar">
								<form action="#">
									<input type="text" placeholder="Search Product">
									<button>Search</button>
								</form> <!-- /form -->
								<h4>Shop Categories :</h4>
								<ul class="Light-Shop">
									<li><a href="#">Home wear</a></li>
									<li><a href="#">Home Textile</a></li>
									<li><a href="#">Garments</a></li>
									<li><a href="#">Home neccesities</a></li>
									<li><a href="#">Watches</a></li>
									<li><a href="#">Cars</a></li>
									<li><a href="#">Children care</a></li>
									<li><a href="#">Home appliances</a></li>
									<li><a href="#">Others</a></li>

								</ul> <!-- /.Light-Shop -->
								<h4>Popular Product</h4>
								<ul class="Popular-Product">
									<li>
										<img src="images/shop/1.jpg" alt="image">
										<h6><a href="#"></a></h6>
										<span>€ </span>
									</li>
									<li>
										<img src="images/shop/2.jpg" alt="image">
										<h6><a href="#"></a></h6>
										<span>€ </span>
									</li>
									<li>
										<img src="images/shop/3.jpg" alt="image">
										<h6><a href="#"></a></h6>
										<span>€ </span>
									</li>
									<li>
										<img src="images/shop/4.jpg" alt="image">
										<h6><a href="#"></a></h6>
										<span>€ </span>
									</li>
								</ul> <!-- /.Popular-Product -->
							</div> <!-- /.Shop-Side-Bar -->
						</div> <!-- /.col -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</section> <!-- /.Shop-Pages -->
			
<?php include('footer.php'); ?>