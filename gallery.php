<?php include('header.php'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
    $(document).ready(function(){
    
    });
    
</script>
			<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Gallery</h1>
								<p>SPONSOR A CHILD AND CHANGE THEIR LIFE FOR <br>GOOD</p>
								<ul>
									<li><a href="index.html">Home</a></li>
									<li><span>-</span></li>
									<li><a href="#">Gallery</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Need Our Help</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>

			<!-- Gallery ____________________________ -->
			<section class="">
				<div class="Gallery-Wrapper container" style="background-color: whitesmoke">
					<div class="mixitUp_menu" >
						<ul >
							<div style="height: 30px"></div>
							<li class="filter tran3s" data-filter="all" style="font-size: 35px!important;">All Images</li>
							<!-- <li class="filter tran3s" data-filter=".Charity ">Charity</li>
							<li class="filter tran3s" data-filter=".Homeless">Homeless</li>
							<li class="filter tran3s" data-filter=".Children-s">Children's</li>
							<li class="filter tran3s" data-filter=".Donation">Donation</li>
							<li class="filter tran3s" data-filter=".Events">Events</li> -->
						</ul>
					</div> <!-- End of .mixitUp_menu -->

					<div class="gallery_item_wrapper row" id="mixitup_list">
						<?php
						$query="SELECT * FROM album as a, images as i WHERE i.album_id=a.id";
						$cmd=mysqli_query($conn,$query);
						while ($row=mysqli_fetch_row($cmd)) {
							$img=$row[3].".".$row[4];
							
						
						 ?>
						<!-- Single Item -->
						<div class="col-sm-3 col-xs-12 mix Charity Children-s Events">
							<div class="portfolio-item">
							    <?php
							    if($row[0]!=39){
							    ?>
								<img src="./album/<?php
											echo $img
											?>" height="200px"   alt="image">
											
											<?php
							    }else{
							        ?>
                                <object width="225" height="225"  style="    width: 270px;max-height:200px;background:#000000;"  
											 ><param id="movie" name="movie" frame="0"   value="" ></param></object>
							        <?php
							    }
											?>
								<div class="item-opacity">
									<div class="text-center">
										<h6><?php  echo $row[1]?></h6>
										<ul>
											<li><a data-fancybox="gallery" href="./album/<?php
											echo $img


											?>" class="tran3s zoom-view" data-caption="Strong Apollo"><i class="flaticon-photo"></i></a></li>
											<li><a href="showAlbum.php?id=<?php echo $row[0]?>"><i class="flaticon-arrow-curve-pointing-to-right"></i></a></li>
										</ul>
									</div> <!-- /.text-center -->
								</div> <!-- /.item-opacity -->
							</div> <!-- /.portfolio-item -->
						</div> <!-- /.col -->
					<?php } ?>
						
						
						
						

						
					</div> <!-- / .row /#mixitup_list -->
				</div> <!-- /.Gallery-Wrapper --> 
			</section> <!-- /.margin-top -->
			
			<!-- Children Care List  _________________________________ -->
			<section class="Children-Care-list-margin margin-top">
				<div class="container">
					<div class="Children-Care-list">
						<div id="Children-Care-List-Slider" class="owl-carousel owl-theme">
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-handshake"></i>
									<h6><a href="#">Children’s Care</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="fa fa-eur"></i>
									<h6><a href="#">Donate</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation"></i>
									<h6><a href="#">Volunteer</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation-2"></i>
									<h6><a href="#">Protect Planet</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
						</div> <!-- / #Children-Care-List-Slider -->
					</div> <!-- /.Children-Care-list -->
				</div> <!-- /.container -->
			</section> <!-- /.Children-Care-list-margin -->

<?php include('footer.php'); ?>