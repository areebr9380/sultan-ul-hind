<?php 
	include('header.php');
 ?>
	
	
	
		<div class="main-page-wrapper">

			<!-- Header _________________________________ -->
			
			<!-- Theme Main Banner ____________________________ -->
			<section>
				<div id="theme-main-banner">
					<div data-src="images/banners/banner-1.png">
						<div class="camera_caption">
							<div class="container text-center">
							    <h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Food Assistance <br>Lets Donate</h1>
							    <h6 class="wow fadeInUp animated" data-wow-delay="0.4s">join today</h6>
								<a href="#" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s">Donation Now</a>
							</div> <!-- /.container -->
						</div> <!-- /.camera_caption -->
					</div>
					<div data-src="images/banners/banner-2.png">
						<div class="camera_caption">
							<div class="container text-center">
								<h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Land For Graveyard <br>Lets Donate</h1>
							    <h6 class="wow fadeInUp animated" data-wow-delay="0.4s">join today</h6>
								<a href="#" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s">Donation Now</a>
							</div> <!-- /.container -->
						</div> <!-- /.camera_caption -->
					</div>
					<div data-src="images/banners/banner-3.png">
						<div class="camera_caption">
							<div class="container text-center">
								<h1 class="wow fadeInUp animated" data-wow-delay="0.1s">Homeless People <br>Lets Donate</h1>
							    <h6 class="wow fadeInUp animated" data-wow-delay="0.4s">join today</h6>
								<a href="#" class="tran3s banner-button wow fadeInUp animated hvr-bounce-to-right" data-wow-delay="0.7s">Donation Now</a>
							</div> <!-- /.container -->
						</div> <!-- /.camera_caption -->
					</div>
				</div> <!-- /#theme-main-banner -->
			</section>

			<!-- Children Care List  _________________________________ -->
			<section class="Children-Care-list-margin">
				<div class="container">
					<div class="Children-Care-list">
						<div id="Children-Care-List-Slider" class="owl-carousel owl-theme">
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-handshake"></i>
									<h6><a href="#">Children’s Care</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation-1"></i>
									<h6><a href="#">Donate</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation"></i>
									<h6><a href="#">Volunteer</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="text-center list-item">
									<i class="flaticon-donation-2"></i>
									<h6><a href="#">Protect Planet</a></h6>
								</div> <!-- /.list-item -->
							</div> <!-- /.item -->
						</div> <!-- / #Children-Care-List-Slider -->
					</div> <!-- /.Children-Care-list -->
				</div> <!-- /.container -->
			</section> <!-- /.Children-Care-list-margin -->

			<!-- Banner Bottom Section _________________________________ -->
			<section class="banner-bottom-section">
				<div class="opact-div">
					<div class="container">
						<div class="row">
							<div class="col-md-9 col-xs-12">
								<div class="banner-bottom-section-text">
									<h3>Make a single or monthly donation today</h3>
									<p>Find out how you can help children affected by poverty, conflict and natural disasters through a one-off donation or a pledge of regular support.</p>
								</div> <!-- /.banner-bottom-section-text -->
							</div> <!-- /.col -->
							<div class="col-md-3 col-xs-12">
								<div class="banner-bottom-section-button clear-fix">
									<div><a href="#" class="hvr-bounce-to-right">Learn More !</a></div>
								</div> <!-- /.banner-bottom-section-button -->
							</div> <!-- /.col -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.opact-div -->
			</section> <!-- /.banner-bottom-section -->

			<!-- You Can Bring ____________________________ -->
			<section class="You-Can-Bring">
				<div class="You-Can-Bring-Title">
					<div class="container">
						<div class="row">
							<div class="col-lg-5 col-sm-6 col-xs-12">
								<h3>You can bring real hope by $</h3>
							</div> <!-- /.col -->
							<div class="col-lg-7 col-sm-6 col-xs-12">
								<p>Your donation powers the Red Cross response to nearly 64,000 disasters a year nationwide, providing shelter, food, emotional support and other necessities to those affected</p>
							</div> <!-- /.col -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.You-Can-Bring-Title -->
				<div class="You-Can-Bring-Item-Wrapper">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<div class="Bring-Item text-center">
								<h3>Help our School Children</h3>
								<p>Be inspired! Take a look at the impact of our programmes which we've achievedthrough your support and donations</p>
								<span>$ 1200 To Go</span>
								<a href="#" class="a-comon hvr-bounce-to-right">Donation Now !</a>
							</div> <!-- /.Bring-Item -->
						</div> <!-- /.col -->
						<div class="col-sm-6  col-xs-12">
							<div class="Bring-Item bring-item-bg-two text-center">
								<h3>EAST AFRICACRISIS APPEAL</h3>
								<p>Be inspired! Take a look at the impact of our programmes which we've achievedthrough your support and donations</p>
								<span>$ 1400 To Go</span>
								<a href="#" class="a-comon hvr-bounce-to-right">Donation Now !</a>
							</div> <!-- /.Bring-Item -->
						</div> <!-- /.col -->
					</div> <!-- /.row -->
				</div> <!-- /.You-Can-Bring-Item-Wrapper -->
			</section> <!-- /.You-Can-Bring -->

			<!-- Rcent Causes ____________________________ -->
			<section class="Rcent-Causes-Section">
				<div class="container">
					<div class="Theme-title text-center">
						<h2>Recent Causes</h2>
						<h6>We need you</h6>
					</div> <!-- /.Theme-title -->

					<div class="Rcent-Causes-Item-Wrapper">
						<div id="Rcent-Causes-Slider" class="owl-carousel owl-theme">
							<div class="item">
								<div class="Causes-Item">
									<div class="Causes-Img"><img src="images/home/img-1.jpg" alt="image"></div> <!-- /.Causes-Img -->
									<div class="Causes-Text">
										<h3>Support a child<br>(Ireland , Pakistan, Somalia, others)</h3>
										<ul>
											<li>Donated</li>
											<li>
												<div class="donate-piechart tran3s">
									                <div class="piechart"  data-border-color="rgba(253,88,11,1)" data-value=".90">
													  <span>.90</span>
													</div>
									            </div> <!-- /.donate-piechart -->
											</li>
											<li>$ 1600 to Go</li>
										</ul>
										<p>Child sponsorship is a unique relationship, that brings real hope and a life-affirming experience.</p>
										<a href="causes-details.html">Donation</a>
									</div> <!-- /.Causes-Text -->
								</div> <!-- /.Causes-Item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="Causes-Item">
									<div class="Causes-Img"><img src="images/home/img-2.jpg" alt="image"></div> <!-- /.Causes-Img -->
									<div class="Causes-Text">
										<h3>Assistance for homeless<br>(Ireland , Pakistan, Somalia, others)</h3>
										<ul>
											<li>Donated</li>
											<li>
												<div class="donate-piechart tran3s">
									                <div class="piechart"  data-border-color="rgba(253,88,11,1)" data-value=".75">
													  <span>.75</span>
													</div>
									            </div> <!-- /.donate-piechart -->
											</li>
											<li>$ 1700 to Go</li>
										</ul>
										<p>Child sponsorship is a unique relationship, that brings real hope and a life-affirming experience.</p>
										<a href="causes-details.html">Donation</a>
									</div> <!-- /.Causes-Text -->
								</div> <!-- /.Causes-Item -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="Causes-Item">
									<div class="Causes-Img"><img src="images/home/img-3.jpg" alt="image"></div> <!-- /.Causes-Img -->
									<div class="Causes-Text">
										<h3>Health Assistance Program <br>(Ireland , Pakistan, Somalia, Syria, Yemen, others)</h3>
										<ul>
											<li>Donated</li>
											<li>
												<div class="donate-piechart tran3s">
									                <div class="piechart"  data-border-color="rgba(253,88,11,1)" data-value=".80">
													  <span>.80</span>
													</div>
									            </div> <!-- /.donate-piechart -->
											</li>
											<li>$ 1500 to Go</li>
										</ul>
										<p>Child sponsorship is a unique relationship, that brings real hope and a life-affirming experience.</p>
										<a href="causes-details.html">Donation</a>
									</div> <!-- /.Causes-Text -->
								</div> <!-- /.Causes-Item -->
							</div> <!-- /.item -->
						</div> <!-- /.row -->
						<a href="causes.php" class="hvr-float-shadow">Load more Causes</a>
					</div> <!-- /.Rcent-Causes-Item-Wrapper -->
				</div> <!-- /.container -->
			</section> <!-- /.Rcent-Causes-Section -->

			<!-- Company History _________________________________ -->
			<section class="company-history-section">
				<div class="company-history-shape-img-top"><img src="images/shape/shape-1.png" alt="shape-img"></div><!-- /.company-history-shape-img-top -->
				<div class="company-history-containt-opact">
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-xs-6 history-item-weight">
								<div class="clear-fix">
									<div class="history-item item-one">
										<div>
											<i class="flaticon-donate"></i>
											<p>Fundrising</p>
											<h2><span class="timer" data-from="0" data-to="1425" data-speed="2000" data-refresh-interval="5">0</span></h2>
										</div>
									</div> <!-- /.history-item -->
								</div> <!-- /.clear-fix -->
							</div> <!-- /.col -->
							<div class="col-lg-3 col-xs-6 history-item-weight">
								<div class="clear-fix">
									<div class="history-item item-two">
										<div>
											<i class="flaticon-group"></i>
											<p>Volunteer</p>
											<h2><span class="timer" data-from="0" data-to="1200" data-speed="2000" data-refresh-interval="5">0</span></h2>
										</div>
									</div> <!-- /.history-item -->
								</div> <!-- /.clear-fix -->
							</div> <!-- /.col -->
							<div class="col-lg-3 col-xs-6 history-item-weight">
								<div class="clear-fix">
									<div class="history-item item-three">
										<div>
											<i class="flaticon-donation-3"></i>
											<p>Donator</p>
											<h2><span class="timer" data-from="0" data-to="201" data-speed="2000" data-refresh-interval="5">0</span></h2>
										</div>
									</div> <!-- /.history-item -->
								</div> <!-- /.clear-fix -->
							</div> <!-- /.col -->
							<div class="col-lg-3 col-xs-6 history-item-weight">
								<div class="clear-fix">
									<div class="history-item item-four">
										<div>
											<i class="flaticon-donation-1"></i>
											<p>Raised Funds</p>
											<h2><span class="timer" data-from="0" data-to="20" data-speed="2000" data-refresh-interval="5">0</span>M</h2>
										</div>
									</div> <!-- /.history-item -->
								</div> <!-- /.clear-fix -->
							</div> <!-- /.col -->
						</div> <!-- /.row -->
					</div> <!-- /.container -->
				</div> <!-- /.company-history-containt-opact -->
				<div class="company-history-shape-img-bottom"><img src="images/shape/shape-2.png" alt="shape-img"></div><!-- /.company-history-shape-img-bottom -->
			</section> <!-- /.company-history-section -->

			<!-- Upcoming Events ____________________________ -->
			<section class="Upcoming-Events">
				<div class="container">
					<div class="Theme-title text-center">
						<h2>Upcoming & Recent Events</h2>
						<h6>Events</h6>
					</div> <!-- /.Theme-title -->

					<ul class="Events-Item-Wrapper">
						<li class="clear-fix">
							<div class="events-img"><img src="images/home/img-4.jpg" alt="image"></div><!-- /.events-img -->
							<div class="day">
								<h2>23</h2>
								<h6>NOVEMBER <br> Monday</h6>
							</div> <!-- /.day -->
							<div class="events-text">
								<h4>Sharing seeds, sparking connection</h4>
								<p><i>24 December 2017, 08:00 - 27 December 2017, 00:00</i> Brooklyn, NY 10036 New York, NY 10036 New York, United States</p>
							</div> <!-- /.events-text -->
							<a href="events-details.html"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
						</li> <!-- /.clear-fix -->
					</ul> <!-- /.Events-Item-Wrapper -->
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="official-charity charity-one-bg-color">
								<h4>Run it forward official charity</h4>
								<p>Every day we bring real hope to millions of children in the world's hardest places as a sign of God's unconditional love</p>
							</div> <!-- /.official-charity -->
						</div> <!-- /.col -->
						<div class="col-md-6 col-xs-12">
							<div class="official-charity">
								<h4>Run it forward official charity</h4>
								<p>Every day we bring real hope to millions of children in the world's hardest places as a sign of God's unconditional love</p>
							</div> <!-- /.official-charity -->
						</div> <!-- /.col -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</section> <!-- /.Upcoming-Events -->

			<!-- testimonial section _________________________________ -->
			<section class="testimonial-section">
				<div class="testimonial-shape-img"><img src="images/shape/shape-3.png" alt="shape-img"></div><!-- /.shape-img -->
				<div class="testimonial-opact">
					<div class="testimonial-containt">
						<div class="container">
							<div class="testimonial-shape-border"><i class="flaticon-right-quotation-sign"></i></div>

							<div id="client-carousel" class="carousel slide" data-ride="carousel" data-interval="2500">
								<!-- Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#client-carousel" data-slide-to="0"></li>
									<li data-target="#client-carousel" data-slide-to="1" class="active"></li>
									<li data-target="#client-carousel" data-slide-to="2"></li>
								</ol>

								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
								    <div class="item">
								     	<p>Every day we bring real hope to millions of children in the world's hardest places as a sign of God's unconditional love.Every day we bring real hope to millions of children in the world's hardest places</p>
								     	<span>Mahfuz R  ( CEO Ch )</span>
								     	<img src="images/home/1.jpg" alt="image">
								    </div> <!-- /.item -->
								    <div class="item active">
								     	<p>Every day we bring real hope to millions of children in the world's hardest places as a sign of God's unconditional love.Every day we bring real hope to millions of children in the world's hardest places</p>
								     	<span>Mahfuz R  ( CEO Ch )</span>
								     	<img src="images/home/1.jpg" alt="image">
								    </div> <!-- /.item -->
								    <div class="item">
								    	<p>Every day we bring real hope to millions of children in the world's hardest places as a sign of God's unconditional love.Every day we bring real hope to millions of children in the world's hardest places</p>
								     	<span>Mahfuz R  ( CEO Ch )</span>
								     	<img src="images/home/1.jpg" alt="image">
								    </div> <!-- /.item -->
								</div> <!-- /.carousel-inner -->
							</div> <!-- Wrapper for bootstrap slides -->
						</div> <!-- /.container -->
					</div> <!-- /.testimonial-containt -->
				</div> <!-- /.testimonial-opact -->
			</section> <!-- /.testimonial-section -->

			<!-- News Update _________________________________ -->
			<section class="news-update-section">
				<div class="container">
					<div class="Theme-title text-center">
						<h2>News Update</h2>
						<h6>Latest News</h6>
					</div> <!-- /.Theme-title -->

					<div class="row home-news-update-wrapper">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="home-news-update-item">
								<div class="news-update-img"><img src="images/home/img-5.jpg" alt="image"></div><!-- /.news-update-img -->
								<div class="news-update-text">
									<span> <i class="fa fa-clock-o" aria-hidden="true"></i> March 4, 2017 1:10 pm</span>
									<p>Monsoon floods: World Vision warns of spike in child</p>
									<a href="blog-v1.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> Learn more</a>
								</div> <!-- /.news-update-text -->
							</div> <!-- /.home-news-update-item -->
						</div> <!-- /.col -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="home-news-update-item">
								<div class="news-update-img"><img src="images/home/img-6.jpg" alt="image"></div><!-- /.news-update-img -->
								<div class="news-update-text">
									<span> <i class="fa fa-clock-o" aria-hidden="true"></i> April 4, 2017 1:10 pm</span>
									<p>Increase in heat-related illness for Iraqi children</p>
									<a href="blog-v1.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> Learn more</a>
								</div> <!-- /.news-update-text -->
							</div> <!-- /.home-news-update-item -->
						</div> <!-- /.col -->
						<div class="col-md-4 col-xs-12">
							<div class="home-news-update-item">
								<div class="news-update-img"><img src="images/home/img-7.jpg" alt="image"></div><!-- /.news-update-img -->
								<div class="news-update-text">
									<span> <i class="fa fa-clock-o" aria-hidden="true"></i> May 4, 2017 1:10 pm</span>
									<p>Charity fears that South Suda-nese refugee children</p>
									<a href="blog-v1.html"><i class="fa fa-arrow-right" aria-hidden="true"></i> Learn more</a>
								</div> <!-- /.news-update-text -->
							</div> <!-- /.home-news-update-item -->
						</div> <!-- /.col -->
					</div> <!-- /.home-news-update-wrapper -->
				</div> <!-- /.container -->
			</section> <!-- /.news-update-section -->

<?php include('footer.php') ?>			

	
