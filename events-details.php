<?php include('header.php') ?>

			<!-- Theme Inner Banner ____________________________ -->
			<section>
				<div class="Theme-Inner-Banner inner-banner-bg-img-one">
					<div class="banner-opacity">
						<div class="container">
							<div class="banner-content">
								<h1>Event Details</h1>
								<p>SPONSOR A CHILD AND CHANGE THEIR LIFE FOR <br>GOOD</p>
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><span>-</span></li>
									<li><a href="#">Event details</a></li>
								</ul>
								<a href="#" class="hvr-bounce-to-right">Need Our Help</a>
							</div> <!-- /.banner-content -->
						</div> <!-- /.container -->
					</div> <!-- /.banner-opacity -->
				</div> <!-- /.Theme-Inner-Banner -->
			</section>

<?php 
	$eventTitle='';
	$eventImage ='';
	$eventDate ='';
	$eventStart ='';
	$eventEnd ='';
	$eventAddress ='';
	$eventDescription ='';
	$monthName='';
	$mapUrl ='';

	if(isset($_GET['eventId'])){
		$getId = $_GET['eventId'];	
		$query = mysqli_query($conn,"SELECT * FROM event Where id = '$getId'");
		
	}
	
	
	while ($rowEvent = mysqli_fetch_array($query)) {
		$eventTitle= $rowEvent['title'];
		$eventImage = $rowEvent['image'].".".$rowEvent['extension'];
		$eventDate = $rowEvent['date'];
		$eventStart = $rowEvent['start'];
		$eventEnd = $rowEvent['end'];
		$eventAddress = $rowEvent['venue'];
		$eventDescription = $rowEvent['description'];
		$mapUrl = $rowEvent['location'];

		$orderdate = explode('-', $eventDate);
		$year 		= $orderdate[0];
		$month 		= $orderdate[1];
		$day  		= $orderdate[2];

		$monthQuery = mysqli_query($conn,"SELECT * FROM month WHERE id ='$month'");
		while ($rowMonth = mysqli_fetch_array($monthQuery)) {
			$monthName = $rowMonth['monthName'];
		}
	}
?>
			<!-- Event-Details-Pages ____________________________ -->
			<section class="Event-Details-Pages">
				<div class="container">
					<div class="Rcent-Causes-Item-Wrapper Causes-Details-Wrapper">
						<div class="row">
							<div class="col-md-8 col-xs-12 margin-top">
								<div class="Causes-Item Causes-Details-Item clear-fix">
									<div style="margin-top:-95px;margin-bottom:15px;"><h2><?php echo $eventTitle;?></h2></div>
									<div class="Causes-Img"><img style="height:670px;" src="events/<?php echo $eventImage;?>" alt="image"></div> <!-- /.Causes-Img -->
									<div class="Causes-Text" style="    height: 133px;">
										
										<ul class="Wacker-Drive">
											<li><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $eventAddress;?></li><br>
											<li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $day."-".$monthName."-".$year.', '.$eventStart.' - '.$day."-".$monthName."-".$year.", ".$eventEnd;?></li>
											<!-- <li><i class="fa fa-cc-diners-club" aria-hidden="true"></i> $45</li> -->
										</ul>
										<!-- <ul class="Look-Event">
											<li><i class="fa fa-calendar-o" aria-hidden="true"></i> All Events + Google Calendar + iCal Export</li>
											<li><a href="#">Look Event on Facebook</a></li>
										</ul> -->
									</div> <!-- /.Causes-Text -->
								</div> <!-- /.Causes-Item -->
								<div class="Details-Info">
									<div class="Share-Div">
										<h3>Event Location</h3>
										<div class="Event-Location-Google-map">
											<?php echo $mapUrl; ?>
											<!-- Google Map _______________________ -->
											<div class="map-canvas"></div>
										</div> <!-- /.Contact-us-Google-map -->
									</div> <!-- /.Share-Div -->
								</div> <!-- /.Details-Info -->
								
								
								
							</div> <!-- /.col -->

							<!-- ================ Right Side Bar ================== -->
							<div class="col-md-4 col-xs-12">
								<div class="Right-Side-Bar">
									<form action="#" class="Side-Search">
										<input type="text" placeholder="Search">
										<button><i class="fa fa-search" aria-hidden="true"></i></button>
									</form> <!-- /.Side-Search -->
									<div class="Side-Upcoming-Events">
										<h5>Upcoming Events</h5>
<?php 
	$upComingEvent = mysqli_query($conn,"SELECT * FROM event WHERE status=1 LIMIT 4");
	while ($rowUpComing = mysqli_fetch_array($upComingEvent)) {
		$upEventTitle= $rowUpComing['title'];
		$upEventImage = $rowUpComing['image'].".".$rowUpComing['extension'];
		$upEventDate = $rowUpComing['date'];
		$upEventStart = $rowUpComing['start'];
		$upEventEnd = $rowUpComing['end'];
		$upEventAddress = $rowUpComing['venue'];
		$upEventDescription = $rowUpComing['description'];

		$orderdate2 = explode('-', $upEventDate);
		$year2 		= $orderdate2[0];
		$month2 	= $orderdate2[1];
		$day2 		= $orderdate2[2];

		$monthQuery2 = mysqli_query($conn,"SELECT * FROM month WHERE id ='$month'");
		while ($rowMonth2 = mysqli_fetch_array($monthQuery2)) {
			$monthName2 = $rowMonth2['monthName'];
?>
										<ul style="background-color: white;">
											<li>
												<img src="events/<?php echo $upEventImage?>" alt="image">
												<div class="text">
													<h6><a href="events-details.php?eventId=<?php echo $rowUpComing['id'];?>"><?php echo $upEventTitle?></a></h6>
													<span><?php echo $day."-".$monthName2."-".$year2;?></span>
												</div>
											</li>
										</ul>
<?php
	}
}
?>					
									</div> <!-- /.Side-Upcoming-Events -->
									
									
								</div> <!-- /.Right-Side-Bar -->
							</div> <!-- /.col -->
						</div> <!-- /.row -->
					</div> <!-- /.Rcent-Causes-Item-Wrapper -->
				</div> <!-- /.container -->
			</section> <!-- /.Event-Details-Pages -->

<?php include('footer.php'); ?>				